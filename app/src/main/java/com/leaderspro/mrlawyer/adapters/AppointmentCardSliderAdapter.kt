package com.leaderspro.mrlawyer.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.util.Log
import android.view.View
import com.github.islamkhsh.CardSliderAdapter
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.ConstantValue
import com.leaderspro.mrlawyer.models.AppointmentModel
import kotlinx.android.synthetic.main.recycler_appointment_item.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class AppointmentCardSliderAdapter(appointment: ArrayList<AppointmentModel>?) :
    CardSliderAdapter<AppointmentModel>(appointment!!) {


    companion object {

        var todayAppointmentCount = 0
    }

    override fun bindView(position: Int, itemContentView: View, item: AppointmentModel?) {
        item?.apply {
            itemContentView.tvClientName.text = clientName
            itemContentView.tvAdvName.text = advName
            itemContentView.tvAppointmentType.text = appointmentType
            itemContentView.tvAppointmentDate.text = timeAndDate.split(" ")[0]
            itemContentView.tvAppointmentTime.text = timeAndDate.split(" ")[1]
            itemContentView.tvEnemies.text = enemies
            itemContentView.tvCourt.text = court
            itemContentView.tvNotes.text = notes


            //change cardBackground color for tomorrow appointment
            if (timeAndDate.split(" ")[0] == getTomorrowDate()) {
                itemContentView.appointmentCardView.setBackgroundColor(Color.parseColor("#ffc001"))
                itemContentView.backgroundCV.setBackgroundColor(Color.parseColor("#ffc001"))
            } else {//Increase todayAppointmentCount
                itemContentView.backgroundCV.setBackgroundColor(Color.parseColor("#ffde01"))
                itemContentView.appointmentCardView.setBackgroundColor(Color.parseColor("#ffde01"))
                todayAppointmentCount++
            }


            itemContentView.ivWhatsapp.setOnClickListener {
                sendEmail((itemContentView.context as Activity))
                //phoneCall((itemContentView.context as Activity))
                //sendWhatsapp((itemContentView.context as Activity), "962789317222", "Hello")
            }

        }

    }

    private fun sendEmail(activity: Activity) {
        Log.e("SSSSSS", "Send email")
        val TO = arrayOf("someone@gmail.com")
        val CC = arrayOf("xyz@gmail.com")
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
        emailIntent.putExtra(Intent.EXTRA_CC, CC)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here")
        try {
            activity.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
            Log.e("SSSSSS", "Finished sending email...")
        } catch (ex: android.content.ActivityNotFoundException) {
            Log.e("SSSSSS", "There is no email client installed...")
        }
    }

    private fun phoneCall(activity: Activity) {
        val callIntent = Intent()
        callIntent.action = Intent.ACTION_CALL
        callIntent.data = Uri.parse("tel:962789317222")//change the number
        activity.startActivity(callIntent)
    }


    private fun sendWhatsapp(activity: Activity, phone: String, message: String) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_VIEW
        val url = "https://api.whatsapp.com/send?phone=$phone&text=$message"
        sendIntent.data = Uri.parse(url)
        activity.startActivity(sendIntent)
    }

    override fun getItemContentLayout(position: Int): Int = R.layout.recycler_appointment_item

    private fun getTomorrowDate(): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow = calendar.time
        val dateFormat: DateFormat = SimpleDateFormat(ConstantValue.dataFormat, Locale.ENGLISH)
        return dateFormat.format(tomorrow).toString()
    }
}