package com.leaderspro.mrlawyer.adapters

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.TODOModel
import kotlinx.android.synthetic.main.recycler_todo_item.view.*
import org.json.JSONObject


class TODOAdapter(private val mArray: ArrayList<TODOModel>, val context: Context) :
    RecyclerView.Adapter<TODOAdapter.ViewHolder>() {

    var mView: View? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mView = LayoutInflater.from(context).inflate(R.layout.recycler_todo_item, parent, false)

        return ViewHolder(mView!!)
    }

    override fun getItemCount() = mArray.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val mTODO = mArray[holder.adapterPosition]


        //Set Recycler for first time
        holder.tvTodoTask.text = mTODO.task

        holder.tvTODODate.text = mTODO.date

        if (mTODO.isDone == "0") {//not complete

            holder.todoListMainLinear.setBackgroundResource(R.drawable.border_radius_yellow)

            holder.ivIsDone.setImageResource(R.drawable.ic_checkbox_unchecked)

            holder.tvTodoTask.paintFlags =
                (holder.tvTodoTask.paintFlags) and (Paint.STRIKE_THRU_TEXT_FLAG.inv())


        } else if (mTODO.isDone == "1") {

            holder.todoListMainLinear.setBackgroundResource(R.drawable.border_radius_red)

            holder.ivIsDone.setImageResource(R.drawable.ic_checkbox_checked)

            holder.tvTodoTask.paintFlags =
                (holder.tvTodoTask.paintFlags) or (Paint.STRIKE_THRU_TEXT_FLAG) //put line on done Tasks

        }




        holder.ivIsDone.setOnClickListener {
            holder.ivIsDone.setImageResource(R.drawable.ic_loading)

            if (mTODO.isDone == "0") {
                updateTodoReq(mTODO.todoId!!, "1", holder.adapterPosition, holder)
            } else {
                updateTodoReq(mTODO.todoId!!, "0", holder.adapterPosition, holder)
            }


        }


    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvTodoTask = itemView.tvTodoTask!!
        val tvTODODate = itemView.tvTODODate!!
        val ivIsDone = itemView.ivIsDone!!

        val todoListMainLinear = itemView.todoListMainLinear!!

    }


    private fun updateTodoReq(
        id: String,
        isDone: String,
        localArrayPosition: Int,
        holder: ViewHolder
    ) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(this.context)

        //login Request
        val updateClientReq =
            object : StringRequest(
                Method.POST,
                API.updateTodo,
                Response.Listener { response ->

                    val jsonObj = JSONObject(response)

                    when (jsonObj.getString("message")) { // there is only one object
                        "success" -> {

                            if (isDone == "1") {

                                mArray[localArrayPosition].isDone = "1"

                                holder.todoListMainLinear.setBackgroundResource(R.drawable.border_radius_red)

                                holder.ivIsDone.setImageResource(R.drawable.ic_checkbox_checked)

                                holder.tvTodoTask.paintFlags =
                                    (holder.tvTodoTask.paintFlags) or (Paint.STRIKE_THRU_TEXT_FLAG) //put line on done Tasks
                            } else {

                                mArray[localArrayPosition].isDone = "0"

                                holder.todoListMainLinear.setBackgroundResource(R.drawable.border_radius_yellow)

                                holder.tvTodoTask.paintFlags =
                                    (holder.tvTodoTask.paintFlags) and (Paint.STRIKE_THRU_TEXT_FLAG.inv())

                                holder.ivIsDone.setImageResource(R.drawable.ic_checkbox_unchecked)
                            }

                            reSortTodoList(mArray)

                            KSnackToast().show(context, "تم تحديث قائمة المهام بنجاح")


                        }
                        "fail" -> {
                            KSnackToast().show(context, "لم يتم تحديث قائمة المهام بنجاح")
                        }
                    }

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context, error.toString())
                }) {

                override fun getParams(): MutableMap<String?, String?> {
                    val params2 = HashMap<String?, String?>()
                    params2["id"] = id
                    params2["isDone"] = isDone

                    return params2
                }

            }
        mRequestQueue!!.add(updateClientReq)

    }

    private fun reSortTodoList(mArray: ArrayList<TODOModel>) {

        val tempArr0 = ArrayList<TODOModel>()
        val tempArr1 = ArrayList<TODOModel>()

        for (x in 0 until mArray.size) {
            if (mArray[x].isDone == "0") {
                tempArr0.add(mArray[x])
            } else {
                tempArr1.add(mArray[x])
            }
        }


        mArray.clear()
        mArray.addAll(tempArr0)
        mArray.addAll(tempArr1)

        tempArr0.clear()
        tempArr1.clear()

        notifyDataSetChanged()
    }

}