package com.leaderspro.mrlawyer.adapters

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.CaseEnemyModel
import kotlinx.android.synthetic.main.recycler_client_case_enemies_item.view.*
import org.json.JSONObject


class CaseEnemiesAdapter(
    private val mArray: ArrayList<CaseEnemyModel>,
    private val context: Context
) :
    RecyclerView.Adapter<CaseEnemiesAdapter.ViewHolder>() {

    var mView: View? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mView = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_client_case_enemies_item, parent, false)

        return ViewHolder(mView!!)
    }

    override fun getItemCount() = mArray.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val enemy = mArray[holder.adapterPosition]


        holder.tvEnemyName.text = enemy.e_name
        holder.tvEnemySid.text = enemy.e_sid
        holder.tvEnemyPhone.text = enemy.e_phone
        holder.tvEnemyAddress.text = enemy.e_address
        holder.tvEnemyAdvName.text = enemy.adv_name
        holder.tvEnemyAdvPhone.text = enemy.adv_phone
        holder.tvEnemyAdvAddress.text = enemy.adv_address
        holder.tvEnemyDescription.text = enemy.enemieDescription


        holder.ivMenu.setOnClickListener {
            val popup = PopupMenu(context, holder.ivMenu)
            popup.menuInflater.inflate(R.menu.menu_item, popup.menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.actionEdit -> {
                        Toast.makeText(context, "Delete", Toast.LENGTH_SHORT).show()

                    }


                    R.id.actionDelete -> {
                        removeEnemy(enemy.id!!)

                    }


                }
                false
            }
            popup.show()

        }

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val ivMenu = itemView.ivMenu!!

        val tvEnemyName = itemView.tvEnemyName!!
        val tvEnemySid = itemView.tvEnemySid!!
        val tvEnemyPhone = itemView.tvEnemyPhone!!
        val tvEnemyAddress = itemView.tvEnemyAddress!!
        val tvEnemyAdvName = itemView.tvEnemyAdvName!!
        val tvEnemyAdvPhone = itemView.tvEnemyAdvPhone!!
        val tvEnemyAdvAddress = itemView.tvEnemyAdvAddress!!
        val tvEnemyDescription = itemView.tvEnemyDescription!!

    }

    private fun removeEnemy(enemyId: String) {

        val mRequestQueue = Volley.newRequestQueue(context as Activity)

        val deleteEnemyReq =
            object : StringRequest(
                Method.POST,
                API.deleteEnemy,
                Response.Listener { response ->

                    Log.e("askjfsf", response)

                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context, "تم حذف الخصم بنجاح")


                            //refresh local enemy array
                            for (x in 0 until mArray.size) {
                                if (mArray[x].id == enemyId) {
                                    mArray.removeAt(x)
                                    break
                                }
                            }
                            notifyDataSetChanged()

                        }
                        "fail" -> {
                            KSnackToast().show(context, "لم يتم حذف الخصم بنجاح")
                        }
                    }

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context, error.toString())
                }) {


                override fun getParams(): MutableMap<String, String> {
                    val params2 = java.util.HashMap<String, String>()
                    params2["enemyId"] = enemyId
                    return params2
                }


            }

        mRequestQueue!!.add(deleteEnemyReq)
    }


    private fun replaceFragment(fragment: Fragment, addToBack: Boolean, TAG: String) {
        val transaction = (context as FragmentActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainerClientsDetails, fragment, TAG)
        if (addToBack) {
            transaction.addToBackStack(TAG)
        } else {
            transaction.addToBackStack(null)
        }

        transaction.commit()

    }


}