package com.leaderspro.mrlawyer.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.CaseProcessModel
import kotlinx.android.synthetic.main.recycler_client_case_process_item.view.*
import org.json.JSONObject


class CaseProcessAdapter(
    private val mArray: ArrayList<CaseProcessModel>,
    private val context: Context
) :
    RecyclerView.Adapter<CaseProcessAdapter.ViewHolder>() {

    var mView: View? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mView = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_client_case_process_item, parent, false)

        return ViewHolder(mView!!)
    }

    override fun getItemCount() = mArray.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val process = mArray[holder.adapterPosition]


        holder.tvProcess.text = process.process
        holder.tvRegisterDate.text = process.register_date
        holder.tvProcessType.text = process.type
        holder.tvProcessStatus.text = process.status

        holder.ivMenu.setOnClickListener {

            val popup = PopupMenu(context, holder.ivMenu)
            popup.menuInflater.inflate(R.menu.menu_item, popup.menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.actionEdit -> {


                    }


                    R.id.actionDelete -> {
                        removeProcess(process.id!!)

                    }


                }
                false
            }
            popup.show()
        }


    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvProcess = itemView.tvProcess!!
        val tvRegisterDate = itemView.tvRegisterDate!!
        val tvProcessType = itemView.tvProcessType!!
        val tvProcessStatus = itemView.tvProcessStatus!!

        val ivMenu = itemView.ivMenu!!
    }


    private fun replaceFragment(fragment: Fragment, addToBack: Boolean, TAG: String) {
        val transaction = (context as FragmentActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainerClientsDetails, fragment, TAG)
        if (addToBack) {
            transaction.addToBackStack(TAG)
        } else {
            transaction.addToBackStack(null)
        }

        transaction.commit()

    }


    private fun removeProcess(processId: String) {

        val mRequestQueue = Volley.newRequestQueue(context)

        val removeProcessReq =
            object : StringRequest(
                Method.POST,
                API.deleteProcess,
                Response.Listener { response ->

                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context, "تم حذف الإجراء بنجاح")


                            //refresh local process Array
                            for (x in 0 until mArray.size) {
                                if (mArray[x].id == processId) {
                                    mArray.removeAt(x)
                                    break
                                }
                            }
                            notifyDataSetChanged()

                        }
                        "fail" -> {
                            KSnackToast().show(context, "لم يتم حذف الإجراء بنجاح")
                        }
                    }

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context, error.toString())
                }) {


                override fun getParams(): MutableMap<String, String> {
                    val params2 = java.util.HashMap<String, String>()
                    params2["id"] = processId
                    return params2
                }


            }

        mRequestQueue!!.add(removeProcessReq)
    }

}