package com.leaderspro.mrlawyer.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.fragments.clientDetailsFragments.ClientsDetailsFragment
import com.leaderspro.mrlawyer.models.ClientModel
import com.leaderspro.mrlawyer.models.ClientModelData
import kotlinx.android.synthetic.main.recycler_client_item.view.*


class ClientsAdapter(private val clientList: ArrayList<ClientModel>, val context: Context) :
    RecyclerView.Adapter<ClientsAdapter.ViewHolder>() {

    companion object{
        var sharedSelectedClient:ClientModelData? = null
    }

    var mView: View? = null

    private val fragmentContainerMainActivity = R.id.fragmentContainerMainActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mView = LayoutInflater.from(context).inflate(R.layout.recycler_client_item, parent, false)
        return ViewHolder(mView!!)
    }

    override fun getItemCount() = clientList[0].data!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        val mClientInfo = clientList[0].data?.get(position)!!


        holder.tvName.text = mClientInfo.fname
        holder.tvClientPhone.text = mClientInfo.phone

        holder.llClientName.setOnClickListener {
            sharedSelectedClient = mClientInfo
            val clientsDetailsFragment = ClientsDetailsFragment()

            (context as FragmentActivity)
                .supportFragmentManager
                .beginTransaction()
                .replace(
                    fragmentContainerMainActivity,
                    clientsDetailsFragment,
                    ClientsDetailsFragment.TAG
                )
                .addToBackStack(ClientsDetailsFragment.TAG)//enable back to ClientFragment
                .commit()

        }

        holder.ivCallClient.setOnClickListener {
            Toast.makeText(context, "Call", Toast.LENGTH_SHORT).show()
        }


    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvName = itemView.tvName!!
        val tvClientPhone = itemView.tvClientPhone!!
        val llClientName = itemView.llClientName!!
        val ivCallClient = itemView.ivCallClient!!


    }


}