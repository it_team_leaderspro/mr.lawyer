package com.leaderspro.mrlawyer.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments.ClientsCasesDetailsFragment
import com.leaderspro.mrlawyer.models.CaseModel
import kotlinx.android.synthetic.main.recycler_client_case_item.view.*
import java.util.*


class ClientCasesAdapter(
    val mArray: ArrayList<CaseModel>,
    private val mContext: Context
) :
    RecyclerView.Adapter<ClientCasesAdapter.ViewHolder>() {

    var mView: View? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mView = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_client_case_item, parent, false)

        return ViewHolder(mView!!)
    }

    override fun getItemCount() = mArray.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mCase = mArray[holder.adapterPosition]

        holder.tvCaseInternalNum.text = mCase.internal_num
        holder.tvCaseNumber.text = mCase.case_num
        holder.tvCaseType.text = mCase.strTypeId
        holder.tvCourt.text = mCase.courtName
        holder.tvJudge.text = mCase.judge


        holder.llClientCase.setOnClickListener {

            replaceFragment(
                ClientsCasesDetailsFragment(mCase),
                true,
                ClientsCasesDetailsFragment.TAG
            )
        }


    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvCaseInternalNum = itemView.tvCaseInternalNum!!
        val tvCaseNumber = itemView.tvCaseNumber!!
        val tvCaseType = itemView.tvCaseType!!
        val tvCourt = itemView.tvCourt!!
        val tvJudge = itemView.tvJudge!!

        val llClientCase = itemView.llClientCase!!

    }


    private fun replaceFragment(fragment: Fragment, addToBack: Boolean, TAG: String) {
        val transaction = (mContext as FragmentActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainerMainActivity, fragment, TAG)

        if (addToBack) {
            transaction.addToBackStack(TAG)
        }

        transaction.commit()

    }


}

