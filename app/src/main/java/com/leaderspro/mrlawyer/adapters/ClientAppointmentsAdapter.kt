package com.leaderspro.mrlawyer.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.RenderProcessGoneDetail
import androidx.recyclerview.widget.RecyclerView
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.models.AppointmentModel
import kotlinx.android.synthetic.main.recycler_appointment_item.view.*


class ClientAppointmentsAdapter(
    val context: Context,
    private val clientAppointmentsList: ArrayList<AppointmentModel>
) :
    RecyclerView.Adapter<ClientAppointmentsAdapter.ViewHolder>() {

    var mView: View? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mView =
            LayoutInflater.from(context).inflate(R.layout.recycler_appointment_item, parent, false)
        return ViewHolder(mView!!)
    }

    override fun getItemCount() = clientAppointmentsList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        val mClientAppointment = clientAppointmentsList[position]


        //hide it
        holder.ivAppointmentType.visibility = View.GONE
        holder.tvClientName.visibility = View.GONE
        holder.ivWhatsapp.visibility = View.GONE
        holder.headerLine.visibility = View.GONE


        holder.tvAdvName.text = mClientAppointment.advName
        holder.tvAppointmentType.text = mClientAppointment.appointmentType
        holder.tvAppointmentDate.text = mClientAppointment.timeAndDate.split(" ")[0]
        holder.tvAppointmentTime.text = mClientAppointment.timeAndDate.split(" ")[1]
        holder.tvEnemies.text = mClientAppointment.enemies
        holder.tvCourt.text = mClientAppointment.court
        holder.tvNotes.text = mClientAppointment.notes


        /* holder.llClientName.setOnClickListener {
             val clientsDetailsFragment = ClientsDetailsFragment(mClientInfo)

             (context as FragmentActivity)
                 .supportFragmentManager
                 .beginTransaction()
                 .replace(container, clientsDetailsFragment)
                 .addToBackStack("clientsDetailsFragment")
                 .commit()

         }*/


/*  holder.ivCallClient.setOnClickListener {
            Toast.makeText(context, "Call", Toast.LENGTH_SHORT).show()
        }*/


    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        //hide it
        val ivAppointmentType=itemView.ivAppointmentType!!
        val tvClientName = itemView.tvClientName!!
        val ivWhatsapp= itemView.ivWhatsapp!!
        val headerLine= itemView.headerLine!!

        val tvAdvName = itemView.tvAdvName!!
        val tvAppointmentType = itemView.tvAppointmentType!!
        val tvAppointmentDate = itemView.tvAppointmentDate!!
        val tvAppointmentTime = itemView.tvAppointmentTime!!
        val tvEnemies = itemView.tvEnemies!!
        val tvCourt = itemView.tvCourt!!
        val tvNotes = itemView.tvNotes!!


    }


}