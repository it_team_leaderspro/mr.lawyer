package com.leaderspro.mrlawyer.fragments.clientDetailsFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.models.ClientModelData
import kotlinx.android.synthetic.main.fragment_clients_details.view.*

/**
 * A simple [Fragment] subclass.
 */
class ClientsDetailsFragment : Fragment() {


    companion object {
        const val TAG = "ClientsDetailsFragment"
        var selectedElasticView = "Info" //Default Fragment
    }


    private val fragmentContainerClientsDetails = R.id.fragmentContainerClientsDetails

    //selected Elastic View
    private val strInfo = "Info"
    private val strAppointment = "Appointment"
    private val strCases = "Cases"
    private val strYearlyAgreements = "Yearly Agreements"


    private var mView: View? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_clients_details, container, false)



        //load default fragment (Info)
        elasticViewSelected(selectedElasticView)


        //Elastics View (Info)
        mView!!.evClientsInfo.setOnClickListener {

            if (selectedElasticView == strInfo)
                return@setOnClickListener

            elasticViewSelected(strInfo)
        }

        //Elastics View (Appointment)
        mView!!.evClientsAppointment.setOnClickListener {

            if (selectedElasticView == strAppointment)
                return@setOnClickListener


            elasticViewSelected(strAppointment)
        }

        //Elastics View (Cases)
        mView!!.evClientsCases.setOnClickListener {
            if (selectedElasticView == strCases)
                return@setOnClickListener

            elasticViewSelected(strCases)
        }

        //Elastics View (Yearly Agreement)
        mView!!.evClientsYearlyAgreements.setOnClickListener {

            if (selectedElasticView == strYearlyAgreements)
                return@setOnClickListener

            elasticViewSelected(strYearlyAgreements)
        }



        return mView
    }


    //change Background of elasticView THEN call setFragment Fun
    private fun elasticViewSelected(strSelected: String) {
        selectedElasticView = strSelected

        when (strSelected) {

            strInfo -> {
                //change elastic view background
                mView!!.evClientsInfo.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_Selected
                )
                mView!!.evClientsAppointment.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsYearlyAgreements.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )
                //End of change elastic view background


                setFragment()


            }//end of info case

            strAppointment -> {
                mView!!.evClientsInfo.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsAppointment.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_Selected
                )
                mView!!.evClientsCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsYearlyAgreements.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )


                setFragment()

            }//end of appointment case

            strCases -> {
                mView!!.evClientsInfo.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsAppointment.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_Selected
                )
                mView!!.evClientsYearlyAgreements.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )

                setFragment()

            }//end of Cases case

            strYearlyAgreements -> {
                mView!!.evClientsInfo.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsAppointment.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evClientsYearlyAgreements.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_Selected
                    )

                setFragment()

            }//end of yearlyAgreements case


        }

    }

    private fun setFragment() {

        val fm = activity!!.supportFragmentManager
        val transaction = fm.beginTransaction()

        var fragment: Fragment? = null
        var tag: String? = null

        when (selectedElasticView) {

            strInfo -> {
                selectedElasticView = strInfo
                fragment = ClientsInfoFragment()
                tag = ClientsInfoFragment.TAG
            }
            strAppointment -> {
                selectedElasticView = strAppointment
                fragment = ClientsAppointmentsFragment()
                tag = ClientsAppointmentsFragment.TAG
            }
            strCases -> {
                selectedElasticView = strCases
                fragment = ClientsCasesFragment()
                tag = ClientsCasesFragment.TAG
            }
            strYearlyAgreements -> {
                selectedElasticView = strYearlyAgreements
                fragment = ClientsYearlyAgreementsFragment()
                tag = ClientsYearlyAgreementsFragment.TAG
            }

        }



        transaction.replace(this.fragmentContainerClientsDetails, fragment!!, tag)
        transaction.commit()

    }

}
