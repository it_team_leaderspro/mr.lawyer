package com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments


import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.ConstantValue
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.CaseProcessModel
import kotlinx.android.synthetic.main.fragment_insert_client_case_process.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class InsertClientCaseProcessFragment(val caseId: String) : Fragment() {

    companion object {
        val TAG = "InsertClientCaseProcessFragment"
    }

    //Process Date
    var cal: Calendar? = null
    var mView: View? = null

    private var spinnerProcessStatusAdapter: ArrayAdapter<String>? = null
    private val processStatusStringArr = ArrayList<String>()
    private val processStatusIdArr = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_insert_client_case_process, container, false)


        //There is no process status in DB

        processStatusIdArr.add("0")
        processStatusStringArr.add("مسجلة")

        processStatusIdArr.add("1")
        processStatusStringArr.add("غير مسجلة")

        spinnerProcessStatusAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, processStatusStringArr)
        mView!!.spinnerProcessStatus.setPositiveButton("رجوع")
        mView!!.spinnerProcessStatus.adapter = spinnerProcessStatusAdapter


        //Later-on Get it from DB
/*

//process Status Spinner
        spinnerProcessStatusAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, processStatusStringArr)
        mView!!.spinnerCourts.setPositiveButton("رجوع")
        processStatusReq()


 */


        //Process date button
        cal = Calendar.getInstance()
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal!!.set(Calendar.YEAR, year)
                cal!!.set(Calendar.MONTH, monthOfYear)
                cal!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat(ConstantValue.dataFormat, Locale.US)

                val strDate = sdf.format(cal!!.time)
                mView!!.tvProcessDate.text = strDate

            }

        mView!!.ivProcessDate.setOnClickListener {
            DatePickerDialog(
                container!!.context,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal!!.get(Calendar.YEAR),
                cal!!.get(Calendar.MONTH),
                cal!!.get(Calendar.DAY_OF_MONTH)
            ).show()
        }





        mView!!.btnAddCaseProcess.setOnClickListener {

            val newProcess = CaseProcessModel()
            if (checkUserInput()) {

                newProcess.process = mView!!.etCaseProcessName.text.toString()
                newProcess.date = mView!!.tvProcessDate.text.toString()

                newProcess.status =
                    processStatusIdArr[mView!!.spinnerProcessStatus.selectedItemPosition]


                insertNewProcessReq(newProcess)
            }
        }



        return mView
    }

    private fun insertNewProcessReq(newProcess: CaseProcessModel) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(activity)

        //login Request initialized
        val insertProcess =
            object :
                StringRequest(Method.POST, API.insertProcess, Response.Listener { response ->

                    //check if successful insert Then close the fragment
                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context!!, "تم إضافة الإجراء")

                        }
                        "fail" -> {
                            KSnackToast().show(context!!, "لم يتم إضافة الإجراء بنجاح")
                        }
                    }

                }, Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["case_id"] = caseId
                    params2["process"] = newProcess.process!!
                    params2["date"] = newProcess.date!!
                    params2["status"] = newProcess.status!!
                    return params2
                }

            }
        mRequestQueue!!.add(insertProcess)


    }


    private fun checkUserInput(): Boolean {


        if (mView!!.etCaseProcessName.text.toString() == "") {
            mView!!.etCaseProcessName.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد إسم الخصم")
            return false
        }

        if (mView!!.tvProcessDate.text.toString() == getString(R.string.emptyDate)) {
            KSnackToast().show(context!!, "الرجاء تحديد تاريخ الإجراء")
            return false
        }


        if (mView!!.spinnerProcessStatus.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد حالة الإجراء")
            return false
        }

        return true
    }


}
