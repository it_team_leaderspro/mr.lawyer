package com.leaderspro.mrlawyer.fragments.clientDetailsFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.adapters.ClientCasesAdapter
import com.leaderspro.mrlawyer.adapters.ClientsAdapter
import com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments.InsertClientCaseFragment
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.CaseModel
import com.leaderspro.mrlawyer.models.ClientModelData
import kotlinx.android.synthetic.main.fragment_clients_appointment.view.*
import kotlinx.android.synthetic.main.fragment_clients_cases.view.*
import org.json.JSONArray

/**
 * A simple [Fragment] subclass.
 */
class ClientsCasesFragment : Fragment() {

    companion object {
        const val TAG = "ClientsCasesFragment"
    }

    var mView: View? = null

    private val casesList = ArrayList<CaseModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        mView = inflater.inflate(R.layout.fragment_clients_cases, container, false)



        mView!!.casesRecycler!!.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        //hide add button when scroll down
        mView!!.casesRecycler!!.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    mView!!.fabAddCases.hide()
                } else {
                    mView!!.fabAddCases.show()
                }
            }
        })


        mView!!.fabAddCases.setOnClickListener {

            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()


            val insertClientCaseFragment = InsertClientCaseFragment()


            fragmentTransaction.replace(
                R.id.fragmentContainerClientsDetails,
                insertClientCaseFragment,
                InsertClientCaseFragment.TAG
            )
            fragmentTransaction.addToBackStack(InsertClientAppointmentFragment.TAG)
            fragmentTransaction.commit()



        }


        //hide add button when scroll down
        mView!!.casesRecycler!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    mView!!.fabAddCases.hide()
                } else {
                    mView!!.fabAddCases.show()
                }
            }
        })


        casesRequest(ClientsAdapter.sharedSelectedClient!!.id!!)

        return mView
    }


    private fun casesRequest(clientId: String) {
        casesList.clear()

        val mRequestQueue = Volley.newRequestQueue(activity)

        val homeReq =
            object : StringRequest(
                Method.POST,
                API.selectAppointmentsCases,
                Response.Listener { response ->

                    val mainArray = JSONArray(response)

                    for (i in 0 until mainArray.length()) {
                        when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                            "Client Cases" -> {
                                when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                    "empty" -> {
                                        KSnackToast().show(
                                            context!!,
                                            "لا يوجد قضايا مسجلة لهذا الموكل"
                                        )
                                    }

                                    "success" -> {

                                        val dataArr =
                                            mainArray.getJSONObject(i).getJSONArray("data")
                                        val caseArr = ArrayList<CaseModel>()

                                        for (x in 0 until dataArr.length()) {
                                            val id = dataArr.getJSONObject(x).getString("id")
                                            val clientID =
                                                dataArr.getJSONObject(x).getString("client_id")
                                            val clientName =
                                                dataArr.getJSONObject(x).getString("clientName")
                                            val courtId =
                                                dataArr.getJSONObject(x).getString("court_id")
                                            val typeId =
                                                dataArr.getJSONObject(x).getString("type_id")
                                            val strTypeId =
                                                dataArr.getJSONObject(x).getString("strTypeId")
                                            val otherType =
                                                dataArr.getJSONObject(x).getString("other_type")
                                            val internalNum =
                                                dataArr.getJSONObject(x).getString("internal_num")
                                            val caseNum =
                                                dataArr.getJSONObject(x).getString("case_num")
                                            val accountNum =
                                                dataArr.getJSONObject(x).getString("account_num")
                                            val judge = dataArr.getJSONObject(x).getString("judge")
                                            val caseType =
                                                dataArr.getJSONObject(x).getString("case_type")
                                            val caseValue =
                                                dataArr.getJSONObject(x).getString("case_value")
                                            val date = dataArr.getJSONObject(x).getString("date")
                                            val decision =
                                                dataArr.getJSONObject(x).getString("decision")
                                            val status =
                                                dataArr.getJSONObject(x).getString("status")
                                            val strCaseStatus =
                                                dataArr.getJSONObject(x).getString("strCaseStatus")
                                            val created =
                                                dataArr.getJSONObject(x).getString("created")
                                            val courtName =
                                                dataArr.getJSONObject(x).getString("courtName")



                                            caseArr.add(
                                                CaseModel(
                                                    id,
                                                    clientID,
                                                    clientName,
                                                    courtId,
                                                    typeId,
                                                    strTypeId,
                                                    otherType,
                                                    internalNum,
                                                    caseNum,
                                                    accountNum,
                                                    judge,
                                                    caseType,
                                                    caseValue,
                                                    date,
                                                    decision,
                                                    status,
                                                    strCaseStatus,
                                                    created,
                                                    courtName
                                                )
                                            )
                                        }

                                        mView!!.casesRecycler!!.adapter =
                                            ClientCasesAdapter(caseArr, context!!)
                                        mView!!.casesRecycler!!.adapter!!.notifyDataSetChanged()


                                    }//end of success case
                                }//end of message(when)
                            }//end of client cases - case


                        }


                    }//end of for-loop (mainArray)

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String?, String?> {
                    val params2 = HashMap<String?, String?>()
                    params2["clientId"] = clientId
                    return params2
                }

            }

        mRequestQueue!!.add(homeReq)
    }

}
