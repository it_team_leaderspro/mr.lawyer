package com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments


import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.adapters.ClientsAdapter
import com.leaderspro.mrlawyer.fragments.clientDetailsFragments.ClientsCasesFragment
import com.leaderspro.mrlawyer.helper.ConstantValue
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.CaseModel
import kotlinx.android.synthetic.main.fragment_insert_client_appointment.view.spinnerCourts
import kotlinx.android.synthetic.main.fragment_insert_client_appointment.view.tvClientName
import kotlinx.android.synthetic.main.fragment_insert_client_case.*
import kotlinx.android.synthetic.main.fragment_insert_client_case.view.*
import kotlinx.android.synthetic.main.fragment_insert_client_case_enemies.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class InsertClientCaseFragment : Fragment() {

    companion object {
        const val TAG = "InsertClientCaseFragment"
    }

    private val fragmentContainerClientsDetails = R.id.fragmentContainerClientsDetails


    var mView: View? = null

    //appointment Date
    private var cal: Calendar? = null


    private var spinnerCourtsAdapter: ArrayAdapter<String>? = null
    val courtStringArr = ArrayList<String>()
    val courtIdArr = ArrayList<String>()

    private var spinnerCaseTypeAdapter: ArrayAdapter<String>? = null
    val caseTypeStringArr = ArrayList<String>()
    val caseTypeIdArr = ArrayList<String>()

    private var spinnerCaseStatusAdapter: ArrayAdapter<String>? = null
    val caseStatusStringArr = ArrayList<String>()
    val caseStatusIdArr = ArrayList<String>()


    private var spinnerEnemyDescriptionAdapter: ArrayAdapter<String>? = null
    val enemyDescriptionStringArr = ArrayList<String>()
    val enemyDescriptionIdArr = ArrayList<String>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_insert_client_case, container, false)

        //clientName
        mView!!.tvClientName.text = ClientsAdapter.sharedSelectedClient!!.fname


        //Courts Spinner
        spinnerCourtsAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, courtStringArr)
        mView!!.spinnerCourts.setPositiveButton("رجوع")
        courtsReq()

        //Case Type Spinner
        spinnerCaseTypeAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, caseTypeStringArr)
        mView!!.spinnerCaseType.setPositiveButton("رجوع")
        caseTypeReq()

        // Case status Spinner
        spinnerCaseStatusAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, caseStatusStringArr)
        mView!!.spinnerCaseStatus.setPositiveButton("رجوع")
        caseStatusReq()


        // Enemy Description Spinner
        spinnerEnemyDescriptionAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, enemyDescriptionStringArr)
        mView!!.spinnerEnemyDescription.setPositiveButton("رجوع")
        enemyDescReq()


        //hide include case enemy(Add Button)
        mView!!.btnAddCaseEnemy.visibility = View.GONE


        //contract date button
        cal = Calendar.getInstance()
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal!!.set(Calendar.YEAR, year)
                cal!!.set(Calendar.MONTH, monthOfYear)
                cal!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat(ConstantValue.dataFormat, Locale.US)

                val strDate = sdf.format(cal!!.time)
                mView!!.tvCaseDate.text = strDate

            }


        mView!!.ivCaseRegisterDate.setOnClickListener {
            DatePickerDialog(
                container!!.context,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal!!.get(Calendar.YEAR),
                cal!!.get(Calendar.MONTH),
                cal!!.get(Calendar.DAY_OF_MONTH)
            ).show()
        }


        mView!!.btnAddCase.setOnClickListener {

            //check user inputs
            if (checkUserInputs()) {
                val mCase = CaseModel()
                mCase.client_id = ClientsAdapter.sharedSelectedClient!!.id
                mCase.court_id = courtIdArr[spinnerCourts.selectedItemPosition]
                mCase.type_id = caseTypeIdArr[spinnerCaseType.selectedItemPosition]
                mCase.other_type = mView!!.etCaseOtherType.text.toString()
                mCase.internal_num = mView!!.etCaseInternalNumber.text.toString()
                mCase.case_num = mView!!.etCaseNum.text.toString()
                mCase.account_num = mView!!.etCaseAccountNumber.text.toString()
                mCase.judge = mView!!.etCaseJudge.text.toString()
                mCase.case_type = mView!!.etCaseType.text.toString()
                mCase.case_value = mView!!.etCaseValue.text.toString()
                mCase.date = mView!!.tvCaseDate.text.toString()
                mCase.decision = mView!!.etCaseDecision.text.toString()
                mCase.status = caseTypeIdArr[spinnerCaseType.selectedItemPosition]

                insertClientCaseReq(mCase)
            }
        }

        return mView


    }

    private fun checkUserInputs(): Boolean {

        if (mView!!.spinnerCourts.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد المحكمة")
            return false
        }

        if (mView!!.spinnerCaseType.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد نوع القضية")
            return false
        }

        if (mView!!.etCaseInternalNumber.text.toString() == "") {
            mView!!.etCaseInternalNumber.error = ""
            KSnackToast().show(context!!, "الرجاء كتابة الرقم الداخلي")

            return false
        }

        if (mView!!.etCaseNum.text.toString() == "") {
            mView!!.etCaseNum.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد رقم الدعوى")

            return false
        }

        if (mView!!.etCaseAccountNumber.text.toString() == "") {
            mView!!.etCaseAccountNumber.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد رقم الحساب")
            return false
        }

        if (mView!!.etCaseJudge.text.toString() == "") {
            mView!!.etCaseJudge.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد الهيئة الحاكمة")
            return false
        }

        if (mView!!.etCaseType.text.toString() == "") {
            mView!!.etCaseType.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد نوع الدعوى")
            return false
        }

        if (mView!!.etCaseValue.text.toString() == "") {
            mView!!.etCaseValue.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد قيمة الدعوى")
            return false
        }


        if (mView!!.tvCaseDate.text == getString(R.string.emptyDate)) {
            KSnackToast().show(context!!, "الرجاء تحديد تاريخ تسجيل القضية")
            return false
        }

        if (mView!!.etCaseLastProcess.text.toString() == "") {
            mView!!.etCaseLastProcess.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد آخر إجراء")

            return false
        }

        if (mView!!.etCaseNextProcess.text.toString() == "") {
            mView!!.etCaseNextProcess.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد الإجراء اللاحق")
            return false
        }

        if (mView!!.etCaseDecision.text.toString() == "") {
            mView!!.etCaseDecision.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد قرار الحكم")
            return false
        }

        if (mView!!.spinnerCaseStatus.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد حالة القضية")
            return false
        }


        //include case enemy
        if (mView!!.etCaseEnemyName.text.toString() == "") {
            mView!!.etCaseEnemyName.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد إسم الخصم")
            return false
        }

        if (mView!!.spinnerEnemyDescription.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد صفة الخصم")
            return false
        }


        return true
    }


    //load court in Spinner
    private fun courtsReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val homeReq =
            object : StringRequest(Method.POST, API.selectCourts, Response.Listener { response ->

                val mainArray = JSONArray(response)

                for (i in 0 until mainArray.length()) {
                    when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                        "Courts" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {
                                    //TODO("If Courts is empty ")
                                }

                                "success" -> { //fill card array list

                                    val mData = mainArray.getJSONObject(i).getJSONArray("data")
                                    for (x in 0 until mData.length()) {

                                        courtIdArr.add(
                                            mData.getJSONObject(x).getString("id")
                                        )
                                        courtStringArr.add(
                                            mData.getJSONObject(x).getString("court")
                                        )

                                    }//end of data loop


                                    // client Adapter layout
                                    mView!!.spinnerCourts.adapter = spinnerCourtsAdapter


                                }//end of success case
                            }//end of message(when)
                        }//end of clients case


                    }


                }//end of for-loop (mainArray)

            }, Response.ErrorListener { error ->
                KSnackToast().show(context!!, error.toString())
            }) {


            }

        mRequestQueue!!.add(homeReq)
    }

    //load case Type in Spinner
    private fun caseTypeReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val caseTypeReq =
            object : StringRequest(Method.POST, API.selectCaseType, Response.Listener { response ->

                val mainArray = JSONArray(response)

                for (i in 0 until mainArray.length()) {
                    when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                        "Case Type" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {
                                    //TODO("If case Type is empty ")
                                }

                                "success" -> { //fill card array list

                                    val mData = mainArray.getJSONObject(i).getJSONArray("data")
                                    for (x in 0 until mData.length()) {

                                        caseTypeIdArr.add(
                                            mData.getJSONObject(x).getString("id")
                                        )
                                        caseTypeStringArr.add(
                                            mData.getJSONObject(x).getString("name_ar")
                                        )

                                    }//end of data loop


                                    // client Adapter layout
                                    mView!!.spinnerCaseType.adapter = spinnerCaseTypeAdapter


                                }//end of success case
                            }//end of message(when)
                        }//end of clients case


                    }


                }//end of for-loop (mainArray)

            }, Response.ErrorListener { error ->
                KSnackToast().show(context!!, error.toString())
            }) {


            }

        mRequestQueue!!.add(caseTypeReq)
    }

    //load case Status in Spinner
    private fun caseStatusReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val caseStatusReq =
            object :
                StringRequest(Method.POST, API.selectCaseStatus, Response.Listener { response ->

                    val mainArray = JSONArray(response)

                    for (i in 0 until mainArray.length()) {
                        when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                            "Case Status" -> {
                                when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                    "empty" -> {
                                        //TODO("If case status is empty ")
                                    }

                                    "success" -> { //fill card array list

                                        val mData = mainArray.getJSONObject(i).getJSONArray("data")
                                        for (x in 0 until mData.length()) {

                                            caseStatusIdArr.add(
                                                mData.getJSONObject(x).getString("id")
                                            )
                                            caseStatusStringArr.add(
                                                mData.getJSONObject(x).getString("status")
                                            )

                                        }//end of data loop


                                        // client Adapter layout
                                        mView!!.spinnerCaseStatus.adapter = spinnerCaseStatusAdapter


                                    }//end of success case
                                }//end of message(when)
                            }//end of clients case


                        }


                    }//end of for-loop (mainArray)

                }, Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {


            }

        mRequestQueue!!.add(caseStatusReq)
    }

    //load Enemy Description in Spinner
    private fun enemyDescReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val enemyDescriptionReq =
            object :
                StringRequest(
                    Method.POST,
                    API.selectEnemyDescription,
                    Response.Listener { response ->

                        val mainArray = JSONArray(response)

                        for (i in 0 until mainArray.length()) {
                            when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                                "Case Description" -> {
                                    when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                        "empty" -> {
                                            //TODO("If enemy desc is empty ")
                                        }

                                        "success" -> { //fill card array list

                                            val mData =
                                                mainArray.getJSONObject(i).getJSONArray("data")
                                            for (x in 0 until mData.length()) {

                                                enemyDescriptionIdArr.add(
                                                    mData.getJSONObject(x).getString("id")
                                                )
                                                enemyDescriptionStringArr.add(
                                                    mData.getJSONObject(x).getString("description")
                                                )

                                            }//end of data loop


                                            // client Adapter layout
                                            mView!!.spinnerEnemyDescription.adapter =
                                                spinnerEnemyDescriptionAdapter


                                        }//end of success case
                                    }//end of message(when)
                                }//end of clients case


                            }


                        }//end of for-loop (mainArray)

                    },
                    Response.ErrorListener { error ->
                        KSnackToast().show(context!!, error.toString())
                    }) {


            }

        mRequestQueue!!.add(enemyDescriptionReq)
    }

    private fun insertClientCaseReq(newCase: CaseModel?) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(activity)

        //login Request initialized
        val insertTodo =
            object :
                StringRequest(Method.POST, API.insertCase, Response.Listener { response ->

                    //check if successful insert Then close the fragment
                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context!!, "تم إضافة القضية بنجاح")
                            activity!!.supportFragmentManager.beginTransaction().remove(this)
                                .replace(R.id.fragmentContainerClientsDetails, ClientsCasesFragment())
                                .commit()


                        }
                        "fail" -> {
                            KSnackToast().show(context!!, "لم يتم إضافة القضية بنجاح")
                        }
                    }

                }, Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["client_id"] = newCase!!.client_id!!
                    params2["court_id"] = newCase.court_id!!
                    params2["type_id"] = newCase.type_id!!
                    params2["other_type"] = newCase.other_type!!
                    params2["internal_num"] = newCase.internal_num!!
                    params2["case_num"] = newCase.case_num!!
                    params2["account_num"] = newCase.account_num!!
                    params2["judge"] = newCase.judge!!
                    params2["case_type"] = newCase.case_type!!
                    params2["case_value"] = newCase.case_value!!
                    params2["date"] = newCase.date!!
                    params2["decision"] = newCase.decision!!
                    params2["status"] = newCase.status!!
                    return params2
                }

            }
        mRequestQueue!!.add(insertTodo)

    }


}
