package com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.leaderspro.mrlawyer.R

/**
 * A simple [Fragment] subclass.
 */
class ClientsSubCases : Fragment() {

    companion object {
        const val TAG = "ClientsSubCases"
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_clients_sub_cases, container, false)
    }


}
