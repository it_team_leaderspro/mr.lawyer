package com.leaderspro.mrlawyer.fragments.clientDetailsFragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.adapters.ClientsAdapter
import com.leaderspro.mrlawyer.fragments.ClientsFragment
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.ClientModelData
import kotlinx.android.synthetic.main.fragment_clients_details_info.view.*
import org.json.JSONObject


/**
 * A simple [Fragment] subclass.
 */
class ClientsInfoFragment : Fragment() {


    companion object {
        const val TAG = "ClientsInfoFragment"
    }

    private var mView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        mView = inflater.inflate(R.layout.fragment_clients_details_info, container, false)


        //edit button
        mView!!.ivClientEdit.setOnClickListener {


            if (!mView!!.tvClientName.isEnabled) {//editable is disabled .. enable it
                mView!!.ivClientEdit.setImageResource(R.drawable.ic_check_gary)
                mView!!.etEmail.isEnabled = true
                mView!!.etPhone.isEnabled = true
                mView!!.tvCourt.isEnabled = true


                //set Focus on First EditText
                mView!!.tvClientName.isEnabled = true
                mView!!.tvClientName.isFocusable = true
                mView!!.tvClientName.isFocusableInTouchMode = true
                mView!!.tvClientName.requestFocus()

                //open Keyboard
                val imm =
                    activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(
                    InputMethodManager.SHOW_IMPLICIT,
                    InputMethodManager.HIDE_IMPLICIT_ONLY
                )
            } else {//editable is enabled .. apply edit and disable it

                val clientId = ClientsAdapter.sharedSelectedClient!!.id!!
                val clientFName = mView!!.tvClientName.text.toString()
                val clientEmail = mView!!.etEmail.text.toString()
                val clientPhone = mView!!.etPhone.text.toString()
                val clientSId = mView!!.tvCourt.text.toString()

              updateClients(
                        clientId,
                        clientFName,
                        clientEmail,
                        clientPhone,
                        clientSId

                )


            }

        }


        //setName
        mView!!.tvClientName.setText(ClientsAdapter.sharedSelectedClient!!.fname)


        //set Email
        mView!!.etEmail.setText(ClientsAdapter.sharedSelectedClient!!.email)
        mView!!.ivClientSendEmail.setOnClickListener {

        }


        //set Client Phone
        mView!!.etPhone.setText(ClientsAdapter.sharedSelectedClient!!.phone)
        mView!!.ivClientCall.setOnClickListener {

        }

        mView!!.ivClientWhatsapp.setOnClickListener {

        }


        mView!!.tvCourt.setText(ClientsAdapter.sharedSelectedClient!!.sid)



        return mView
    }


    private fun updateClients(
        clientId: String,
        clientFName: String,
        clientEmail: String,
        clientPhone: String,
        clientSId: String
    ) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(activity)

        //login Request
        val updateClientReq =
            object : StringRequest(
                Method.POST,
                API.updateClient,
                Response.Listener { response ->

                    val jsonObj = JSONObject(response)

                    when (jsonObj.getString("message")) { // there is only one object
                        "success" -> {
                            KSnackToast().show(context!!,"تم التعديل بنجاح")

                            mView!!.ivClientEdit.setImageResource(R.drawable.ic_edit_gray)
                            mView!!.tvClientName.isEnabled = false
                            mView!!.etEmail.isEnabled = false
                            mView!!.etPhone.isEnabled = false
                            mView!!.tvCourt.isEnabled = false


                            //close keyboard
                            val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(mView!!.windowToken, 0)


                        }
                        "fail" -> {
                            KSnackToast().show(context!!,"لم يتم التعديل بنجاح")

                        }
                    }

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!,error.toString())
                }) {

                override fun getParams(): MutableMap<String?, String?> {
                    val params2 = HashMap<String?, String?>()
                    params2["id"] = clientId
                    params2["fname"] = clientFName
                    params2["email"] = clientEmail
                    params2["phone"] = clientPhone
                    params2["sid"] = clientSId

                    return params2
                }

            }
        mRequestQueue!!.add(updateClientReq)

    }



}
