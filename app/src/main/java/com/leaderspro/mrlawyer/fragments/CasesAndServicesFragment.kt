package com.leaderspro.mrlawyer.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.leaderspro.mrlawyer.R

class CasesAndServicesFragment : Fragment() {

    companion object {
        const val TAG = "CasesAndServicesFragment"
    }

    var mView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_cases_and_services, container, false)


        return mView
    }


}
