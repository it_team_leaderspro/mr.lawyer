package com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.CaseCostModel
import kotlinx.android.synthetic.main.fragment_clients_cases_invoices.view.*
import org.json.JSONArray
import org.json.JSONObject


/**
 * A simple [Fragment] subclass.
 */
class ClientsCasesInvoices(val caseId: String) : Fragment() {

    companion object {
        const val TAG = "ClientsCasesInvoices"
    }

    var mView: View? = null

    var isCostInserted = false
    val mCaseCost = CaseCostModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_clients_cases_invoices, container, false)


        selectCaseCost()

        mView!!.ivMenu.setOnClickListener {


            if (mView!!.ivApplyEdit.visibility == View.GONE) {

                val popup = PopupMenu(context!!, mView!!.ivMenu)
                popup.menuInflater.inflate(R.menu.menu_edit_item, popup.menu)
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {

                        R.id.actionEdit -> {
                            //show apply check
                            mView!!.ivApplyEdit.visibility = View.VISIBLE
                            enableEditText()
                        }


                    }
                    false
                }
                popup.show()


            }
        }


        mView!!.ivApplyEdit.setOnClickListener {

            if (!isCostInserted) {//insert cost for the first time

                val c = getTextFromEditText()
                insertCaseCostReq(c)

            } else {//update Exist cost

                val c = getTextFromEditText()
                updateCaseCostReq(c)
            }
        }


        //invoices

        //get all invoices to recycler view
        //TODO()


        mView!!.fabAddInvoice.setOnClickListener {

            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()


            val fragmentContainerClients = R.id.fragmentContainerClientsCaseDetails
            val insertClientsCasesInvoicesFragment = InsertClientsCasesInvoicesFragment(caseId)


            fragmentTransaction.add(
                fragmentContainerClients,
                insertClientsCasesInvoicesFragment,
                InsertClientsCasesInvoicesFragment.TAG
            )
            fragmentTransaction.addToBackStack(InsertClientsCasesInvoicesFragment.TAG)
            fragmentTransaction.commit()

        }







        return mView
    }


    private fun selectCaseCost() {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(activity)

        //login Request
        val selectCaseCost =
            object : StringRequest(
                Method.POST,
                API.selectCaseCost,
                Response.Listener { response ->

                    val mainArray = JSONArray(response)

                    for (i in 0 until mainArray.length()) {
                        when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                            "Case Cost" -> {
                                when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                    "empty" -> {
                                        isCostInserted = false
                                        KSnackToast().show(context!!, "لم يتم تحديد التكاليف بعد")
                                    }

                                    "success" -> { //fill card array list
                                        isCostInserted = true

                                        val dataArr =
                                            mainArray.getJSONObject(i).getJSONArray("data")
                                        //appointment model

                                        for (x in 0 until dataArr.length()) {
                                            mCaseCost.id = dataArr.getJSONObject(x).getString("id")
                                            mCaseCost.case_id =
                                                dataArr.getJSONObject(x).getString("case_id")
                                            mCaseCost.cost =
                                                dataArr.getJSONObject(x).getString("cost")
                                            mCaseCost.fees =
                                                dataArr.getJSONObject(x).getString("fees")
                                            mCaseCost.execution_fees =
                                                dataArr.getJSONObject(x).getString("execution_fees")
                                            mCaseCost.execution_expenses = dataArr.getJSONObject(x)
                                                .getString("execution_expenses")
                                            mCaseCost.ads =
                                                dataArr.getJSONObject(x).getString("ads")
                                            mCaseCost.spending =
                                                dataArr.getJSONObject(x).getString("spending")
                                            mCaseCost.unique_fees =
                                                dataArr.getJSONObject(x).getString("unique_fees")
                                            mCaseCost.resume_fees =
                                                dataArr.getJSONObject(x).getString("resume_fees")
                                            mCaseCost.experience_fees = dataArr.getJSONObject(x)
                                                .getString("experience_fees")
                                            mCaseCost.auction_fees =
                                                dataArr.getJSONObject(x).getString("auction_fees")
                                            mCaseCost.stamps =
                                                dataArr.getJSONObject(x).getString("stamps")
                                            mCaseCost.other =
                                                dataArr.getJSONObject(x).getString("other")
                                            mCaseCost.offer =
                                                dataArr.getJSONObject(x).getString("offer")
                                        }

                                        setCaseCostTextView()


                                    }//end of success case
                                }//end of message(when)
                            }//end of clients case


                        }


                    }//end of for-loop (mainArray)

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String?, String?> {
                    val params2 = HashMap<String?, String?>()
                    params2["case_id"] = caseId
                    return params2
                }

            }
        mRequestQueue!!.add(selectCaseCost)

    }

    private fun insertCaseCostReq(newCost: CaseCostModel) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(activity)

        //login Request initialized
        val insertCost =
            object :
                StringRequest(Method.POST, API.insertCaseCost, Response.Listener { response ->


                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context!!, "تم إضافة المصاريف بنجاح")
                            disableEditText()
                            mView!!.ivApplyEdit.visibility = View.GONE
                            selectCaseCost()//to get new cost with Id if user want to edit anything
                        }
                        "fail" -> {
                            KSnackToast().show(context!!, "لم يتم إضافة المصاريف بنجاح")
                        }

                    }

                }, Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = java.util.HashMap<String, String>()
                    params2["case_id"] = caseId
                    params2["cost"] = newCost.cost!!
                    params2["fees"] = newCost.fees!!
                    params2["execution_fees"] = newCost.execution_fees!!
                    params2["execution_expenses"] = newCost.execution_expenses!!
                    params2["ads"] = newCost.ads!!
                    params2["spending"] = newCost.spending!!
                    params2["unique_fees"] = newCost.unique_fees!!
                    params2["resume_fees"] = newCost.resume_fees!!
                    params2["experience_fees"] = newCost.experience_fees!!
                    params2["auction_fees"] = newCost.auction_fees!!
                    params2["stamps"] = newCost.stamps!!
                    params2["other"] = newCost.other!!
                    return params2
                }

            }
        mRequestQueue!!.add(insertCost)

    }

    private fun updateCaseCostReq(newCost: CaseCostModel) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(activity)

        //login Request initialized
        val insertCost =
            object :
                StringRequest(Method.POST, API.updateCaseCost, Response.Listener { response ->


                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context!!, "تم تعديل المصاريف بنجاح")
                            disableEditText()
                            mView!!.ivApplyEdit.visibility = View.GONE
                        }
                        "fail" -> {
                            KSnackToast().show(context!!, "لم يتم تعديل المصاريف بنجاح")
                        }

                    }

                }, Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = java.util.HashMap<String, String>()
                    params2["id"] = mCaseCost.id!!
                    params2["cost"] = newCost.cost!!
                    params2["fees"] = newCost.fees!!
                    params2["execution_fees"] = newCost.execution_fees!!
                    params2["execution_expenses"] = newCost.execution_expenses!!
                    params2["ads"] = newCost.ads!!
                    params2["spending"] = newCost.spending!!
                    params2["unique_fees"] = newCost.unique_fees!!
                    params2["resume_fees"] = newCost.resume_fees!!
                    params2["experience_fees"] = newCost.experience_fees!!
                    params2["auction_fees"] = newCost.auction_fees!!
                    params2["stamps"] = newCost.stamps!!
                    params2["other"] = newCost.other!!
                    return params2
                }

            }
        mRequestQueue!!.add(insertCost)

    }


    private fun setCaseCostTextView() {
        mView!!.tvCaseCost.setText(mCaseCost.cost)
        mView!!.tvCostFees.setText(mCaseCost.fees)
        mView!!.tvCostSpending.setText(mCaseCost.spending)
        mView!!.tvCostExecutionFees.setText(mCaseCost.execution_fees)
        mView!!.tvCostExecutionExpenses.setText(mCaseCost.execution_expenses)
        mView!!.tvCostAds.setText(mCaseCost.ads)
        mView!!.tvCostUniqueFees.setText(mCaseCost.unique_fees)
        mView!!.tvCostResumeFees.setText(mCaseCost.resume_fees)
        mView!!.tvCostExperienceFees.setText(mCaseCost.experience_fees)
        mView!!.tvCostAuctionFees.setText(mCaseCost.auction_fees)
        mView!!.tvCostStamps.setText(mCaseCost.stamps)
        mView!!.tvCostOther.setText(mCaseCost.other)

    }

    private fun enableEditText() {
        //enable edit text
        mView!!.tvCaseCost.isEnabled = true
        //set Focus on first editable
        mView!!.tvCaseCost.isFocusable = true
        mView!!.tvCaseCost.isFocusableInTouchMode = true
        mView!!.tvCaseCost.requestFocus()
        openKeyboard()


        mView!!.tvCostFees.isEnabled = true
        mView!!.tvCostSpending.isEnabled = true
        mView!!.tvCostExecutionFees.isEnabled = true
        mView!!.tvCostExecutionExpenses.isEnabled = true
        mView!!.tvCostAds.isEnabled = true
        mView!!.tvCostUniqueFees.isEnabled = true
        mView!!.tvCostResumeFees.isEnabled = true
        mView!!.tvCostExperienceFees.isEnabled = true
        mView!!.tvCostAuctionFees.isEnabled = true
        mView!!.tvCostStamps.isEnabled = true
        mView!!.tvCostOther.isEnabled = true

        openKeyboard()
    }

    private fun disableEditText() {

        //enable edit text
        mView!!.tvCaseCost.isEnabled = false
        //set Focus on first editable
        mView!!.tvCaseCost.isFocusable = false
        mView!!.tvCaseCost.isFocusableInTouchMode = false
        //mView!!.tvCaseCost.requestFocus()


        mView!!.tvCostFees.isEnabled = false
        mView!!.tvCostSpending.isEnabled = false
        mView!!.tvCostExecutionFees.isEnabled = false
        mView!!.tvCostExecutionExpenses.isEnabled = false
        mView!!.tvCostAds.isEnabled = false
        mView!!.tvCostUniqueFees.isEnabled = false
        mView!!.tvCostResumeFees.isEnabled = false
        mView!!.tvCostExperienceFees.isEnabled = false
        mView!!.tvCostAuctionFees.isEnabled = false
        mView!!.tvCostStamps.isEnabled = false
        mView!!.tvCostOther.isEnabled = false

        closeKeyboard()

    }


    //TODO(keyboard not show automatically)
    private fun openKeyboard() {
        //open Keyboard
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)

    }

    private fun closeKeyboard() {
        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mView!!.windowToken, 0)

    }

    private fun getTextFromEditText(): CaseCostModel {
        val mCost = CaseCostModel()

        mCost.cost = mView!!.tvCaseCost.text.toString()
        mCost.fees = mView!!.tvCostFees.text.toString()
        mCost.spending = mView!!.tvCostSpending.text.toString()
        mCost.execution_fees = mView!!.tvCostExecutionFees.text.toString()

        mCost.execution_expenses = mView!!.tvCostExecutionExpenses.text.toString()
        mCost.ads = mView!!.tvCostAds.text.toString()
        mCost.unique_fees = mView!!.tvCostUniqueFees.text.toString()
        mCost.resume_fees = mView!!.tvCostResumeFees.text.toString()

        mCost.experience_fees = mView!!.tvCostExperienceFees.text.toString()
        mCost.auction_fees = mView!!.tvCostAuctionFees.text.toString()
        mCost.stamps = mView!!.tvCostStamps.text.toString()
        mCost.other = mView!!.tvCostOther.text.toString()

        return mCost
    }


}
