package com.leaderspro.mrlawyer.fragments.clientDetailsFragments


import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.adapters.ClientsAdapter
import com.leaderspro.mrlawyer.helper.ConstantValue
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.ClientModelData
import com.leaderspro.mrlawyer.models.UserModel
import kotlinx.android.synthetic.main.fragment_insert_client.view.spinnerLawyers
import kotlinx.android.synthetic.main.fragment_insert_client_appointment.*
import kotlinx.android.synthetic.main.fragment_insert_client_appointment.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class InsertClientAppointmentFragment : Fragment() {

    companion object {
        const val TAG = "InsertClientAppointmentFragment"
    }

    private val fragmentContainerClientsDetails = R.id.fragmentContainerClientsDetails

    var mView: View? = null

    //appointment Date
    private var cal: Calendar? = null

    private var spinnerLawyersAdapter: ArrayAdapter<String>? = null
    val lawyerStringArr = ArrayList<String>()
    val lawyerIdArr = ArrayList<String>()


    private var spinnerCourtsAdapter: ArrayAdapter<String>? = null
    val courtStringArr = ArrayList<String>()
    val courtIdArr = ArrayList<String>()


    private var spinnerCasesNumbersAdapter: ArrayAdapter<String>? = null
    val casesNumbersStringArr = ArrayList<String>()
    //val casesNumbersIdArr = ArrayList<String>()


    private var spinnerAppointmentTypeAdapter: ArrayAdapter<String>? = null
    val appointmentTypeStringArr = ArrayList<String>()
    val appointmentTypeIdArr = ArrayList<String>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_insert_client_appointment, container, false)

        //clientName
        mView!!.tvClientName.text = ClientsAdapter.sharedSelectedClient!!.fname


        //Lawyer Spinner
        spinnerLawyersAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, lawyerStringArr)
        mView!!.spinnerLawyers.setPositiveButton("رجوع")

        lawyersReq()


        //Courts Spinner
        spinnerCourtsAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, courtStringArr)
        mView!!.spinnerCourts.setPositiveButton("رجوع")

        courtsReq()


        //cases Numbers Spinner
        spinnerCasesNumbersAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, casesNumbersStringArr)
        mView!!.spinnerCaseNumber.setPositiveButton("رجوع")

        casesNumbersReq(ClientsAdapter.sharedSelectedClient!!.id!!)


        //Appointments Type Spinner
        spinnerAppointmentTypeAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, appointmentTypeStringArr)
        mView!!.spinnerAppointmentType.setPositiveButton("رجوع")

        //appointmentTypeReq() //TODO(check appointment type in database)


        //contract date button
        cal = Calendar.getInstance()
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal!!.set(Calendar.YEAR, year)
                cal!!.set(Calendar.MONTH, monthOfYear)
                cal!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat(ConstantValue.dataFormat, Locale.US)

                val strDate = sdf.format(cal!!.time)
                mView!!.tvAppointmentDate.text = strDate

            }


        mView!!.ivAppointmentDate.setOnClickListener {
            DatePickerDialog(
                container!!.context,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal!!.get(Calendar.YEAR),
                cal!!.get(Calendar.MONTH),
                cal!!.get(Calendar.DAY_OF_MONTH)
            ).show()
        }


        mView!!.btnAddClientAppointment.setOnClickListener {

            //check user inputs
            if (checkUserInputs()) {
                val date = mView!!.tvAppointmentDate.text.toString()
                val advId = lawyerIdArr[spinnerLawyers.selectedItemPosition]
                val clientId = ClientsAdapter.sharedSelectedClient!!.id
                val courtId = courtIdArr[spinnerCourts.selectedItemPosition]
                val caseNumber = casesNumbersStringArr[spinnerCaseNumber.selectedItemPosition]
                val appointmentType =
                    "3" //appointmentTypeIdArr[spinnerAppointmentType.selectedItemPosition]
                val aucationNum = "" //TODO(check it later)
                val notes = mView!!.etAppointmentNotes.text.toString()

                insertClientAppointment(
                    date,
                    advId,
                    clientId!!,
                    courtId,
                    caseNumber,
                    appointmentType,
                    aucationNum,
                    notes
                )
            }
        }

        return mView


    }

    private fun checkUserInputs(): Boolean {

        if (mView!!.spinnerLawyers.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد المحامي المسؤول")
            return false
        }

        if (mView!!.spinnerCourts.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد المحكمة")
            return false
        }

        if (mView!!.spinnerCaseNumber.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد رقم القضية")
            return false
        }

        /* if (mView!!.spinnerAppointmentType.selectedItemPosition == -1) {
             KSnackToast().show(context!!, "الرجاء تحديد نوع الموعد")
             return false
         }*/


        if (mView!!.tvAppointmentDate.text == getString(R.string.emptyDate)) {
            KSnackToast().show(context!!, "الرجاء تحديد تاريخ الموعد")
            return false
        }


        return true
    }


    //load lawyer in Spinner
    private fun lawyersReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val homeReq =
            object : StringRequest(Method.GET, API.selectLawyers, Response.Listener { response ->

                val mainArray = JSONArray(response)

                for (i in 0 until mainArray.length()) {
                    when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                        "Lawyers" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {
                                    //TODO("If lawyers is empty ")
                                }

                                "success" -> { //fill card array list

                                    val mData = mainArray.getJSONObject(i).getJSONArray("data")
                                    for (x in 0 until mData.length()) {

                                        lawyerIdArr.add(
                                            mData.getJSONObject(x).getString(
                                                UserModel.COLUMN_ID
                                            )
                                        )
                                        lawyerStringArr.add(
                                            mData.getJSONObject(x).getString(
                                                UserModel.COLUMN_fName
                                            )
                                        )


                                    }//end of data loop

                                    // client Adapter layout
                                    mView!!.spinnerLawyers.adapter = spinnerLawyersAdapter


                                }//end of success case
                            }//end of message(when)
                        }//end of clients case


                    }


                }//end of for-loop (mainArray)

            }, Response.ErrorListener { error ->
                KSnackToast().show(context!!, error.toString())
            }) {


            }

        mRequestQueue!!.add(homeReq)
    }


    //load court in Spinner
    private fun courtsReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val homeReq =
            object : StringRequest(Method.POST, API.selectCourts, Response.Listener { response ->

                val mainArray = JSONArray(response)

                for (i in 0 until mainArray.length()) {
                    when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                        "Courts" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {
                                    //TODO("If Courts is empty ")
                                }

                                "success" -> { //fill card array list

                                    val mData = mainArray.getJSONObject(i).getJSONArray("data")
                                    for (x in 0 until mData.length()) {

                                        courtIdArr.add(
                                            mData.getJSONObject(x).getString("id")
                                        )
                                        courtStringArr.add(
                                            mData.getJSONObject(x).getString("court")
                                        )

                                    }//end of data loop


                                    // client Adapter layout
                                    mView!!.spinnerCourts.adapter = spinnerCourtsAdapter


                                }//end of success case
                            }//end of message(when)
                        }//end of clients case


                    }


                }//end of for-loop (mainArray)

            }, Response.ErrorListener { error ->
                KSnackToast().show(context!!, error.toString())
            }) {


            }

        mRequestQueue!!.add(homeReq)
    }

    //load case number in Spinner
    private fun casesNumbersReq(clientId: String) {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val clientCaseNumberReq =
            object : StringRequest(
                Method.POST,
                API.selectClientCaseNumber,
                Response.Listener { response ->

                    val mainArray = JSONArray(response)

                    for (i in 0 until mainArray.length()) {
                        when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                            "Client Case Number" -> {
                                when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                    "empty" -> {
                                        //TODO("If caseNumber is empty ")
                                    }

                                    "success" -> { //fill card array list

                                        val mData = mainArray.getJSONObject(i).getJSONArray("data")
                                        for (x in 0 until mData.length()) {

                                            casesNumbersStringArr.add(
                                                mData.getJSONObject(x).getString("case_num")
                                            )

                                        }//end of data loop

                                        mView!!.spinnerCaseNumber.adapter =
                                            spinnerCasesNumbersAdapter


                                    }//end of success case
                                }//end of message(when)
                            }//end of clients case number


                        }


                    }//end of for-loop (mainArray)

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String?, String?> {
                    val params2 = HashMap<String?, String?>()
                    params2["clientId"] = clientId
                    return params2
                }
            }

        mRequestQueue!!.add(clientCaseNumberReq)
    }


    //load appointment Type in Spinner
    private fun appointmentTypeReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val homeReq =
            object : StringRequest(
                Method.POST,
                API.selectAppointmentsType,
                Response.Listener { response ->

                    val mainArray = JSONArray(response)

                    for (i in 0 until mainArray.length()) {
                        when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                            "Lawyers" -> {
                                when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                    "empty" -> {
                                        //TODO("If Appointment Type is empty ")
                                    }

                                    "success" -> { //fill card array list

                                        val mData = mainArray.getJSONObject(i).getJSONArray("data")
                                        for (x in 0 until mData.length()) {

                                            appointmentTypeIdArr.add(
                                                mData.getJSONObject(x).getString("id")
                                            )
                                            appointmentTypeStringArr.add(
                                                mData.getJSONObject(x).getString("name")
                                            )

                                        }//end of data loop


                                        // client Adapter layout
                                        mView!!.spinnerAppointmentType.adapter =
                                            spinnerAppointmentTypeAdapter


                                    }//end of success case
                                }//end of message(when)
                            }//end of clients case


                        }


                    }//end of for-loop (mainArray)

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {


            }

        mRequestQueue!!.add(homeReq)
    }


    private fun insertClientAppointment(
        date: String,
        advId: String,
        clientId: String,
        courtId: String,
        caseNumber: String,
        appointmentType: String,
        aucationNum: String,
        notes: String
    ) {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val homeReq =
            object : StringRequest(
                Method.POST,
                API.insertClientAppointment,
                Response.Listener { response ->

                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context!!, "تم إضافة الموعد بنجاح")
                            activity!!.supportFragmentManager.beginTransaction().remove(this)
                                .commit()

                            //activity!!.supportFragmentManager.findFragmentByTag("Client Appointment")!!
                            //replace -Insert Fragment- with new -client appointment Fragment-
                            activity!!.supportFragmentManager.beginTransaction().replace(
                                this.fragmentContainerClientsDetails,
                                ClientsAppointmentsFragment(),
                                ClientsAppointmentsFragment.TAG
                            )
                                .commit()

                            //TODO(add task to recyclerView)


                        }
                        "fail" -> {
                            KSnackToast().show(context!!, "لم يتم إضافة الموعد بنجاح")
                        }
                    }

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {


                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["date"] = date
                    params2["advId"] = advId
                    params2["clientId"] = clientId
                    params2["courtId"] = courtId
                    params2["caseId"] = caseNumber
                    params2["appointmentType"] = appointmentType
                    params2["aucationNum"] = aucationNum
                    params2["notes"] = notes
                    return params2
                }


            }

        mRequestQueue!!.add(homeReq)
    }


}
