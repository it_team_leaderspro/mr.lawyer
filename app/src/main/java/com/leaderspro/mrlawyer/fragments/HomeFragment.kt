package com.leaderspro.mrlawyer.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SlidingDrawer
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.adapters.AppointmentCardSliderAdapter
import com.leaderspro.mrlawyer.adapters.TODOAdapter
import com.leaderspro.mrlawyer.helper.ConstantValue
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.AppointmentModel
import com.leaderspro.mrlawyer.models.TODOModel
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.json.JSONArray
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment() {

    companion object {
        const val TAG = "HomeFragment"
    }

    private var mRequestQueue: RequestQueue? = null
    var mView: View? = null

    val appointmentsList = ArrayList<AppointmentModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_home, container, false)
        //todoRecyclerView = mView!!.findViewById(R.id.todoRecyclerView) as RecyclerView

        //RequestQueue initialized
        mRequestQueue = Volley.newRequestQueue(activity)



        weatherRequest()



        homeRequest()

        initialBottomSlide()



        return mView
    }


    private fun initialBottomSlide() {

        val bottomSliding = mView!!.findViewById<SlidingDrawer>((R.id.bottomSlidingDrawer))
        val ivBottomSlidingHandler = mView!!.findViewById<ImageView>((R.id.ivBottomSlidingHandler))


        bottomSliding.setOnDrawerOpenListener {
            ivBottomSlidingHandler.setImageResource(R.drawable.ic_keyboard_arrow_down)
        }

        bottomSliding.setOnDrawerCloseListener {
            ivBottomSlidingHandler.setImageResource(R.drawable.ic_keyboard_arrow_up)

            //check if inner fragment is open
            val fm = activity!!.supportFragmentManager
            val transaction = fm.beginTransaction()

            val mFragment =
                fm.findFragmentByTag(InsertTodoFragment.TAG) //find if insertFragment is exist in container

            if (mFragment != null) {
                transaction.remove(mFragment)
                transaction.commit()
            }

        }


        //Initialize recyclerView
        mView!!.todoRecyclerView!!.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)


        //add new To_Do
        mView!!.ivAddTODO.setOnClickListener {
            val fm = activity!!.supportFragmentManager
            val transaction = fm.beginTransaction()


            val insertTodoFragment = InsertTodoFragment() //create new fragment
            transaction.add(
                R.id.insertContainer,
                insertTodoFragment,
                InsertTodoFragment.TAG
            )

            transaction.addToBackStack(null)
            transaction.commit()

        }


    }


    private fun homeRequest() {

        //For Appointments card
        val mShimmerViewContainer = mView!!.shimmer_view_container
        val appointmentViewPager = mView!!.appointmentViewPager

        val homeReq =
            object : StringRequest(Method.POST, API.home, Response.Listener { response ->

                val mainArray = JSONArray(response)

                for (i in 0 until mainArray.length()) {
                    when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                        "Appointments" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {

                                    /*
                                    //show loading post
                                    mShimmerViewContainer.visibility = View.VISIBLE //Default
                                    //hide appointment viewPager
                                    appointmentViewPager.visibility = View.GONE // Default
                                    */
                                }

                                "success" -> { //fill card array list

                                    mShimmerViewContainer.visibility = View.GONE
                                    //hide appointment viewPager
                                    appointmentViewPager.visibility = View.VISIBLE

                                    //Moshi Library

                                    /* val b = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                                     val adp =
                                         b.adapter<AppointmentData>(AppointmentData::class.java)
                                     val e = adp.fromJson(mainArray.getJSONObject(i).toString())

                                     Log.e("Home", e.toString())

                                     for (x in 0 until mainArray.getJSONObject(i).getJSONArray("data").length()) {
                                         // appointment Adapter layout
                                         mView!!.viewPager.adapter =
                                             AppointmentCardSliderAdapter(e!!)
                                     }*/


                                    val dataArr = mainArray.getJSONObject(i).getJSONArray("data")
                                    //appointment model

                                    for (x in 0 until dataArr.length()) {
                                        val clientName = dataArr.getJSONObject(x).get("clientName")
                                        val advName = dataArr.getJSONObject(x).get("advName")
                                        val court = dataArr.getJSONObject(x).get("court")
                                        val timeAndDate =
                                            dataArr.getJSONObject(x).get("timeAndDate")
                                        val notes = dataArr.getJSONObject(x).get("notes")



                                        appointmentsList.add(
                                            AppointmentModel(
                                                "$clientName",
                                                "$advName",
                                                "<نوع الموعد>",
                                                "$timeAndDate",
                                                "<الخصوم>",
                                                "$court",
                                                "$notes"
                                            )
                                        )

                                    }//end of for loop (dataArray)

                                    // appointment Adapter layout
                                    mView!!.appointmentViewPager.adapter =
                                        AppointmentCardSliderAdapter(appointmentsList)

                                    //set tvTodayAppointmentCount .. value come from Adapter
                                    //using counter effect function
                                    counterEffect(
                                        AppointmentCardSliderAdapter.todayAppointmentCount,
                                        mView!!.tvTodayAppointmentCount
                                    )


                                }//end of success case
                            }//end of message(when)
                        }//end of appointment case

                        "cases_count" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {//cases = 0
                                    mView!!.tvCasesCount.text = "0"
                                }

                                "success" -> { //fill card array list cases > 0
                                    val cCount = mainArray.getJSONObject(i).getJSONArray("data")
                                        .getJSONObject(0).getString("case_count")

                                    //set tvCasesCount using counter effect function
                                    counterEffect(cCount.toInt(), mView!!.tvCasesCount)

                                }
                            }

                        }//end of case_count

                        "to_do" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {//to_do = 0
                                    //TODO(if todo =0)
                                }

                                "success" -> {
                                    val dataArr = mainArray.getJSONObject(i).getJSONArray("data")
                                    val todoArr = ArrayList<TODOModel>()

                                    for (x in 0 until dataArr.length()) {
                                        val id = dataArr.getJSONObject(x).getString("id")
                                        val userName =
                                            dataArr.getJSONObject(x).getString("userId")
                                        val task = dataArr.getJSONObject(x).getString("task")
                                        val isDone = dataArr.getJSONObject(x).getString("isDone")
                                        val date = dataArr.getJSONObject(x).getString("date")


                                        todoArr.add(TODOModel(id, userName, task, isDone, date))
                                    }

                                    mView!!.todoRecyclerView!!.adapter =
                                        TODOAdapter(todoArr, context!!)
                                    mView!!.todoRecyclerView!!.adapter!!.notifyDataSetChanged()


                                }
                            }

                        }//end of to_do

                    }


                }//end of for (mainArray)

            }, Response.ErrorListener { error ->
                KSnackToast().show(context!!, error.toString())
            }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["date1"] = getDate().split("|")[0]//today
                    params2["date2"] = getDate().split("|")[1]//tomorrow
                    return params2
                }

            }

        mRequestQueue!!.add(homeReq)
    }

    private fun weatherRequest() {
        val kelvin = 273

        // get the temp from the server
        val weatherReq = JsonObjectRequest(
            Request.Method.GET, API.weather, null,
            Response.Listener { response ->
                mView!!.temp.text =
                    (response.getJSONArray("list").getJSONObject(0).getJSONObject("main").getInt("temp") - kelvin).toString()
            }, Response.ErrorListener {
                mView!!.temp.text = "0"
            })
        mRequestQueue!!.add(weatherReq)
    }


    fun getDate(): String {

        val calendar = Calendar.getInstance()
        val today = calendar.time

        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow = calendar.time

        val dateFormat: DateFormat = SimpleDateFormat(ConstantValue.dataFormat, Locale.ENGLISH)

        val strToday: String = dateFormat.format(today)
        val strTomorrow: String = dateFormat.format(tomorrow)


        return "$strToday|$strTomorrow"

    }

    fun counterEffect(max: Int, tv: TextView) {
        var counter = 0

        Thread(Runnable {
            while (counter < max) {
                try {
                    Thread.sleep(70)
                } catch (e: InterruptedException) { // TODO Auto-generated catch block
                    e.printStackTrace()
                }
                tv.post { tv.text = counter.toString() }
                counter++
            }
        }).start()
    }

}