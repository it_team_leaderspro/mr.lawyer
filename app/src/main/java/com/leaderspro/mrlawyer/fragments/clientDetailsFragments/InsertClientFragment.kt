package com.leaderspro.mrlawyer.fragments.clientDetailsFragments


import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.ConstantValue
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.UserModel
import kotlinx.android.synthetic.main.fragment_insert_client.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

/**
 * A simple [Fragment] subclass.
 */
class InsertClientFragment : Fragment() {

    companion object {
        const val TAG = "InsertClientFragment"
    }

    //for contract Date
    var cal: Calendar? = null

    var tempType = "-1"


    val spinnerLawyersId = ArrayList<String>()
    val spinnerLawyersNames = ArrayList<String>()

    var spinnerAdapter: ArrayAdapter<String>? = null

    var mView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_insert_client, container, false)


        //first option in spinner
        //mView!!.lawyersSpinner2.setTitle("Title")
        mView!!.spinnerLawyers.setPositiveButton("رجوع")

        spinnerAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, spinnerLawyersNames)


        lawyerReq()


        //contract date button
        cal = Calendar.getInstance()
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal!!.set(Calendar.YEAR, year)
                cal!!.set(Calendar.MONTH, monthOfYear)
                cal!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat(ConstantValue.dataFormat, Locale.US)

                val strDate = sdf.format(cal!!.time)
                mView!!.tvContractDate.text = strDate

            }

        mView!!.ivContractDate.setOnClickListener {
            DatePickerDialog(
                container!!.context,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal!!.get(Calendar.YEAR),
                cal!!.get(Calendar.MONTH),
                cal!!.get(Calendar.DAY_OF_MONTH)
            ).show()
        }


        //Client Type
        mView!!.clientTypeRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            val radio: RadioButton = mView!!.findViewById(checkedId)

            when (radio.text) {
                "فردي" -> {
                    this.tempType = "1"
                }
                "شركة" -> {
                    this.tempType = "2"
                }

            }
        }


        mView!!.btnAddClient.setOnClickListener {

            //TODO(check inputs)
            if (!checkInputs()) {
                KSnackToast().show(context!!, "يرجى التأكد من إدخال كافة البيانات بشكل صحيح")
                return@setOnClickListener
            }


            val fName = mView!!.etFName.text.toString()
            val lName = mView!!.etLName.text.toString()
            val sId = mView!!.etSID.text.toString()
            val phone = mView!!.etPhone.text.toString()
            val email = mView!!.etEmail.text.toString()
            val password = mView!!.etPassword.text.toString()
            val address = mView!!.etAddress.text.toString()


            //get Adv Id
            val advId =
                spinnerLawyersId[mView!!.spinnerLawyers.selectedItemPosition]


            //Client Type
            var type = tempType


            //get contractDate
            // create an OnDateSetListener

            val contractDate = mView!!.tvContractDate.text.toString()


            val yearlyCost = mView!!.etYearlyCost.text.toString()


            //Insert Client request
            insertClientReq(
                fName,
                lName,
                address,
                advId,
                sId,
                phone,
                email,
                password,
                type!!,
                contractDate,
                yearlyCost
            )
        }


        return mView


    }

    //load lawyer in Spinner
    private fun lawyerReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val homeReq =
            object : StringRequest(Method.GET, API.selectLawyers, Response.Listener { response ->

                val mainArray = JSONArray(response)

                for (i in 0 until mainArray.length()) {
                    when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                        "Lawyers" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {
                                    //TODO("If lawyers is empty")
                                }

                                "success" -> { //fill card array list

                                    //Moshi Library
                                    /*val b = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                                    val adp =
                                        b.adapter<ClientModel>(ClientModel::class.java)
                                    val e =
                                        adp.fromJson(mainArray.getJSONObject(i).toString()) //model
                                    clientsList.add(e!!)*/

                                    val mData = mainArray.getJSONObject(i).getJSONArray("data")
                                    for (x in 0 until mData.length()) {

                                        spinnerLawyersId.add(
                                            mData.getJSONObject(x).getString(
                                                UserModel.COLUMN_ID
                                            )
                                        )
                                        spinnerLawyersNames.add(
                                            mData.getJSONObject(x).getString(
                                                UserModel.COLUMN_fName
                                            )
                                        )


                                    }//end of data loop

                                    // client Adapter layout
                                    mView!!.spinnerLawyers.adapter = spinnerAdapter



                                }//end of success case
                            }//end of message(when)
                        }//end of clients case


                    }


                }//end of for-loop (mainArray)

            }, Response.ErrorListener { error ->
                KSnackToast().show(context!!, error.toString())
            }) {


            }

        mRequestQueue!!.add(homeReq)
    }

    private fun insertClientReq(
        fName: String,
        lName: String,
        address: String,
        advId: String,
        sId: String,
        phone: String,
        email: String,
        password: String,
        type: String,
        contractDate: String,
        yearlyCost: String
    ) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(activity)

        //login Request initialized
        val insertTodo =
            object :
                StringRequest(Method.POST, API.insertClient, Response.Listener { response ->

                    //TODO(check if successful insert Then close the fragment)

                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(
                                context!!,
                                getString(R.string.clientAddedSuccessfully)
                            )
                            activity!!.supportFragmentManager.beginTransaction().remove(this)
                                .commit()

                            //re-requestClient to update recyclerView
                            //TODO(Swipe Recycler to refresh)
                            //TODO(add task to recyclerView)

                        }
                        "fail" -> {
                            KSnackToast().show(
                                context!!,
                                getString(R.string.clientAddedUnsuccessfully)
                            )
                        }

                        "Client already exist" -> {
                            KSnackToast().show(context!!, getString(R.string.clientAlreadyExist))

                        }
                    }

                }, Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["fname"] = fName
                    params2["lname"] = lName
                    params2["address"] = address
                    params2["adv_id"] = advId
                    params2["sid"] = sId
                    params2["phone"] = phone
                    params2["email"] = email
                    params2["password"] = password
                    params2["type"] = type
                    params2["contract_date"] = contractDate
                    params2["yearly_cost"] = yearlyCost
                    return params2
                }

            }
        mRequestQueue!!.add(insertTodo)

    }


    private fun checkInputs(): Boolean {

        if (mView!!.etFName.text.toString() == "") {
            mView!!.etFName.error = ""
            return false
        }

        if (mView!!.etLName.text.toString() == "") {
            mView!!.etLName.error = ""
            return false
        }

        if (mView!!.etSID.text.toString() == "") {
            mView!!.etSID.error = ""
            return false
        }


        if (mView!!.etPhone.text.toString() == "") {
            mView!!.etPhone.error = ""
            return false
        }

        if (mView!!.etEmail.text.toString() == "") {
            mView!!.etEmail.error = ""
            return false
        }

        if (mView!!.etPassword.text.toString() == "") {
            mView!!.etPassword.error = ""
            return false
        }

        if (mView!!.etAddress.text.toString() == "") {
            mView!!.etAddress.error = ""
            return false
        }


        //check AdvId Spinner
        if (mView!!.spinnerLawyers.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد المحامي المسؤول")
            return false
        }



        //client Type
        if (this.tempType == "-1") {
            KSnackToast().show(context!!, "يرجى تحديد نوع الموكل")
            return false
        }


        if (mView!!.tvContractDate.text == getString(R.string.emptyDate)) {
            KSnackToast().show(context!!, "الرجاء تحديد تاريخ العقد")
            return false
        }



        if (mView!!.etYearlyCost.text.toString() == "") {
            mView!!.etYearlyCost.error = ""
            return false
        }



        return true
    }
}
