package com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.models.CaseModel
import kotlinx.android.synthetic.main.fragment_client_cases_details.view.*

/**
 * A simple [Fragment] subclass.
 */
class ClientsCasesDetailsFragment(
    private val mCaseInfo: CaseModel
) : Fragment() {

    companion object {
        const val TAG = "ClientsCasesDetailsFragment"
    }

    //selected Elastic View
    private var selectedElasticView: String? = null
    private val strCaseDetails = "Case Details"
    private val strStages = "Stages"
    private val strSubCases = "Sub Cases"
    private val strInvoices = "Invoices"
    private val strAttachments = "Attachments"


    //create new fragment
    private val fragmentContainerClientsCaseDetails = R.id.fragmentContainerClientsCaseDetails
    private val clientsCasesFullDetails = ClientsCasesFullDetails(this.mCaseInfo)
    private val clientsCasesStages = ClientsCasesStages()
    private val clientsSubCases = ClientsSubCases()
    private val clientsCasesInvoices = ClientsCasesInvoices(mCaseInfo.id!!)
    private val clientsCasesAttachments = ClientsCasesAttachments()


    private var mView: View? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_client_cases_details, container, false)


        //load default fragment (full details)(التفاصيل)
        val fm = activity!!.supportFragmentManager
        val transaction = fm.beginTransaction()
        transaction.add(
            fragmentContainerClientsCaseDetails,
            clientsCasesFullDetails,
            ClientsCasesFullDetails.TAG
        ).commit()


        //Elastics View
        mView!!.evCaseDetails.setOnClickListener {

            if (selectedElasticView == strCaseDetails)
                return@setOnClickListener

            elasticViewSelected(strCaseDetails)
        }

        mView!!.evCaseStages.setOnClickListener {

            if (selectedElasticView == strStages)
                return@setOnClickListener

            elasticViewSelected(strStages)
        }

        mView!!.evSubCases.setOnClickListener {
            if (selectedElasticView == strSubCases)
                return@setOnClickListener

            elasticViewSelected(strSubCases)
        }

        mView!!.evCaseInvoices.setOnClickListener {

            if (selectedElasticView == strInvoices)
                return@setOnClickListener


            elasticViewSelected(strInvoices)
        }

        mView!!.evCaseAttachments.setOnClickListener {

            if (selectedElasticView == strAttachments)
                return@setOnClickListener


            elasticViewSelected(strAttachments)
        }



        return mView
    }


    private fun elasticViewSelected(strSelected: String) {

        when (strSelected) {

            strCaseDetails -> {
                selectedElasticView = strCaseDetails

                //change elastic view background
                mView!!.evCaseDetails.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_Selected
                )
                mView!!.evCaseStages.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evSubCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evCaseInvoices.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )

                mView!!.evCaseAttachments.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )
                //End of change elastic view background


                replaceFragment(this.clientsCasesFullDetails, false, ClientsCasesFullDetails.TAG)


            }//end of strCaseDetails case

            strStages -> {
                selectedElasticView = strStages


                mView!!.evCaseStages.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_Selected
                )
                mView!!.evCaseDetails.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evSubCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evCaseInvoices.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )

                mView!!.evCaseAttachments.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )



                replaceFragment(this.clientsCasesStages, false, ClientsCasesStages.TAG)


            }//end of strStages case

            strSubCases -> {
                selectedElasticView = strSubCases

                //change elastic view background
                mView!!.evSubCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_Selected
                )
                mView!!.evCaseStages.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evCaseDetails.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evCaseInvoices.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )

                mView!!.evCaseAttachments.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )



                replaceFragment(this.clientsSubCases, false, ClientsSubCases.TAG)


            }//end of strSubCases case

            strInvoices -> {
                selectedElasticView = strInvoices

                //change elastic view background
                mView!!.evCaseInvoices.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_Selected
                )
                mView!!.evCaseStages.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evSubCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evCaseDetails.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )

                mView!!.evCaseAttachments.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )


                replaceFragment(this.clientsCasesInvoices, false, ClientsCasesInvoices.TAG)


            }//end of strInvoices case

            strAttachments -> {
                selectedElasticView = strAttachments

                //change elastic view background
                mView!!.evCaseAttachments.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_Selected
                )
                mView!!.evCaseStages.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evSubCases.backgroundTintList = ContextCompat.getColorStateList(
                    mView!!.context,
                    R.color.elasticView_notSelected
                )
                mView!!.evCaseInvoices.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )

                mView!!.evCaseDetails.backgroundTintList =
                    ContextCompat.getColorStateList(
                        mView!!.context,
                        R.color.elasticView_notSelected
                    )



                replaceFragment(this.clientsCasesAttachments, false, ClientsCasesAttachments.TAG)


            }//end of strAttachments case


        }

    }

    private fun replaceFragment(fragment: Fragment, addToBack: Boolean, TAG: String) {
        val fm = activity!!.supportFragmentManager
        val transaction = fm.beginTransaction()

        transaction.replace(this.fragmentContainerClientsCaseDetails, fragment, TAG)
        if (addToBack) {
            transaction.addToBackStack(TAG)
        } else {
            transaction.addToBackStack(null)
        }

        transaction.commit()

    }


}
