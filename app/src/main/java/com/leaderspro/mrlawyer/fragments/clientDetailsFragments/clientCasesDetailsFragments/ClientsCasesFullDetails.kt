package com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.github.aakira.expandablelayout.ExpandableLayoutListener
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.adapters.CaseEnemiesAdapter
import com.leaderspro.mrlawyer.adapters.CaseProcessAdapter
import com.leaderspro.mrlawyer.fragments.clientDetailsFragments.ClientsDetailsFragment
import com.leaderspro.mrlawyer.fragments.clientDetailsFragments.InsertClientAppointmentFragment
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.CaseEnemyModel
import com.leaderspro.mrlawyer.models.CaseModel
import com.leaderspro.mrlawyer.models.CaseProcessModel
import kotlinx.android.synthetic.main.fragment_clients_cases_full_details.view.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class ClientsCasesFullDetails(
    private val mCase: CaseModel
) : Fragment() {

    companion object {
        const val TAG = "ClientsCasesFullDetails"
    }

    private var mView: View? = null


    val enemiesArr = ArrayList<CaseEnemyModel>()
    val processArr = ArrayList<CaseProcessModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_clients_cases_full_details, container, false)


        //initial Expandable
        initialExpandable()

        //initial Details
        initialDetails()

        //initial Enemies
        initialEnemies(mCase.id)
        //fabAddEnemy
        mView!!.fabAddEnemy.setOnClickListener {


            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()


            val fragmentContainerClients = R.id.fragmentContainerClientsCaseDetails
            val insertClientCaseEnemiesFragment = InsertClientCaseEnemiesFragment(mCase.id!!)


            fragmentTransaction.add(
                fragmentContainerClients,
                insertClientCaseEnemiesFragment,
                InsertClientCaseEnemiesFragment.TAG
            )
            fragmentTransaction.addToBackStack(InsertClientCaseEnemiesFragment.TAG)
            fragmentTransaction.commit()
        }


        //initial Process
        initialProcess(mCase.id)
        mView!!.fabAddProcess.setOnClickListener {


            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()


            val fragmentContainerClients = R.id.fragmentContainerClientsCaseDetails
            val insertClientCaseProcessFragment = InsertClientCaseProcessFragment(mCase.id!!)


            fragmentTransaction.add(
                fragmentContainerClients,
                insertClientCaseProcessFragment,
                InsertClientCaseProcessFragment.TAG
            )
            fragmentTransaction.addToBackStack(InsertClientCaseProcessFragment.TAG)
            fragmentTransaction.commit()
        }

        return mView
    }


    private fun initialDetails() {
        mView!!.tvClientName.text = mCase.clientName
        mView!!.tvCourt.text = mCase.courtName
        mView!!.tvCaseType.text = mCase.strTypeId
        mView!!.tvCaseOtherType.text = mCase.other_type
        mView!!.tvCaseInternalNum.text = mCase.internal_num
        mView!!.tvCaseNumber.text = mCase.case_num
        mView!!.tvAccountNumber.text = mCase.account_num
        mView!!.tvJudge.text = mCase.judge
        mView!!.tvSuitType.text = mCase.case_type
        mView!!.tvCaseValue.text = mCase.case_value
        mView!!.tvCaseCreatedDate.text = mCase.date
        mView!!.tvDecision.text = mCase.decision
        mView!!.tvCaseStatus.text = mCase.strCaseStatus


        mView!!.ivMenu.setOnClickListener {
            val popupMenu = PopupMenu(context!!, mView!!.ivMenu)
            popupMenu.menuInflater.inflate(R.menu.menu_item, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { item ->

                when (item.itemId) {

                    R.id.actionEdit -> {


                        val fragmentManager = activity!!.supportFragmentManager
                        val fragmentTransaction = fragmentManager.beginTransaction()


                        val insertClientCaseFragment = InsertClientCaseFragment()


                        fragmentTransaction.replace(
                            R.id.fragmentContainerClientsCaseDetails,
                            insertClientCaseFragment,
                            InsertClientCaseFragment.TAG
                        )
                        fragmentTransaction.addToBackStack(InsertClientAppointmentFragment.TAG)
                        fragmentTransaction.commit()


                    }

                    R.id.actionDelete ->
                        deleteCase(mCase.id!!)
                }
                true
            }
            popupMenu.show()

        }

    }

    private fun initialEnemies(caseId: String?) {

        mView!!.enemiesRecyclerView!!.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)


        enemiesArr.clear()

        val mRequestQueue = Volley.newRequestQueue(activity)

        val enemiesReq =
            object : StringRequest(
                Method.POST,
                API.selectEnemies,
                Response.Listener { response ->

                    val mainArray = JSONArray(response)

                    for (i in 0 until mainArray.length()) {
                        when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                            "Case Enemies" -> {
                                when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                    "empty" -> {
                                        //TODO("there is no enemies for this case")
                                    }

                                    "success" -> {

                                        val dataArr =
                                            mainArray.getJSONObject(i).getJSONArray("data")

                                        for (x in 0 until dataArr.length()) {
                                            val id = dataArr.getJSONObject(x).getString("id")
                                            val caseID =
                                                dataArr.getJSONObject(x).getString("case_id")
                                            val eName =
                                                dataArr.getJSONObject(x).getString("e_name")
                                            val eSid =
                                                dataArr.getJSONObject(x).getString("e_sid")
                                            val ePhone =
                                                dataArr.getJSONObject(x).getString("e_phone")
                                            val eAddress =
                                                dataArr.getJSONObject(x).getString("e_address")
                                            val advName =
                                                dataArr.getJSONObject(x).getString("adv_name")
                                            val advPhone =
                                                dataArr.getJSONObject(x).getString("adv_phone")
                                            val advAddress =
                                                dataArr.getJSONObject(x).getString("adv_address")
                                            val descriptionId =
                                                dataArr.getJSONObject(x).getString("description_id")
                                            val enemieDescription = dataArr.getJSONObject(x)
                                                .getString("enemieDescription")




                                            this.enemiesArr.add(
                                                CaseEnemyModel(
                                                    id,
                                                    caseID,
                                                    eName,
                                                    eSid,
                                                    ePhone,
                                                    eAddress,
                                                    advName,
                                                    advPhone,
                                                    advAddress,
                                                    descriptionId,
                                                    enemieDescription
                                                )
                                            )
                                        }

                                        mView!!.enemiesRecyclerView!!.adapter =
                                            CaseEnemiesAdapter(this.enemiesArr, context!!)
                                        mView!!.enemiesRecyclerView!!.adapter!!.notifyDataSetChanged()

                                    }//end of success case
                                }//end of message(when)
                            }//end of client cases - case


                        }


                    }//end of for-loop (mainArray)

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String?, String?> {
                    val params2 = HashMap<String?, String?>()
                    params2["caseId"] = caseId
                    return params2
                }

            }

        mRequestQueue!!.add(enemiesReq)

    }

    private fun initialProcess(caseId: String?) {

        mView!!.processRecyclerView!!.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)


        processArr.clear()

        val mRequestQueue = Volley.newRequestQueue(activity)

        val processReq =
            object : StringRequest(
                Method.POST,
                API.selectProcess,
                Response.Listener { response ->

                    val mainArray = JSONArray(response)

                    for (i in 0 until mainArray.length()) {
                        when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                            "Case Process" -> {
                                when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                    "empty" -> {
                                        //TODO("there is no Process for this case")
                                    }

                                    "success" -> {

                                        val dataArr =
                                            mainArray.getJSONObject(i).getJSONArray("data")

                                        for (x in 0 until dataArr.length()) {
                                            val id = dataArr.getJSONObject(x).getString("id")
                                            val caseID =
                                                dataArr.getJSONObject(x).getString("case_id")
                                            val process =
                                                dataArr.getJSONObject(x).getString("process")
                                            val isNext =
                                                dataArr.getJSONObject(x).getString("is_next")
                                            val date =
                                                dataArr.getJSONObject(x).getString("date")
                                            val registerDate =
                                                dataArr.getJSONObject(x).getString("register_date")
                                            val status =
                                                dataArr.getJSONObject(x).getString("status")
                                            val type =
                                                dataArr.getJSONObject(x).getString("type")





                                            this.processArr.add(
                                                CaseProcessModel(
                                                    id,
                                                    caseID,
                                                    process,
                                                    isNext,
                                                    date,
                                                    registerDate,
                                                    status,
                                                    type
                                                )
                                            )
                                        }

                                        mView!!.processRecyclerView!!.adapter =
                                            CaseProcessAdapter(this.processArr, context!!)
                                        mView!!.processRecyclerView!!.adapter!!.notifyDataSetChanged()

                                    }//end of success case
                                }//end of message(when)
                            }//end of client cases - case


                        }


                    }//end of for-loop (mainArray)

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String?, String?> {
                    val params2 = HashMap<String?, String?>()
                    params2["caseId"] = caseId
                    return params2
                }

            }

        mRequestQueue!!.add(processReq)

    }


    private fun initialExpandable() {

        //close all expandable
        //Details
        mView!!.expandDetails.collapse()
        mView!!.expandDetailsArrow.setImageResource(R.drawable.ic_keyboard_arrow_down)

        //Enemies
        mView!!.expandEnemies.collapse()

        mView!!.expandEnemiesArrow.setImageResource(R.drawable.ic_keyboard_arrow_down)

        //Process
        mView!!.expandProcess.collapse()
        mView!!.expandProcessArrow.setImageResource(R.drawable.ic_keyboard_arrow_down)


        //Set Expandable Listener
        //Details
        mView!!.expandDetails.setListener(object : ExpandableLayoutListener {
            override fun onAnimationEnd() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onOpened() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

            }

            override fun onAnimationStart() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPreOpen() {
                mView!!.expandDetailsArrow.setImageResource(R.drawable.ic_keyboard_arrow_up)
            }

            override fun onClosed() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPreClose() {
                mView!!.expandDetailsArrow.setImageResource(R.drawable.ic_keyboard_arrow_down)
            }

        })

        //Enemies
        mView!!.expandEnemies.setListener(object : ExpandableLayoutListener {
            override fun onAnimationEnd() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onOpened() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

            }

            override fun onAnimationStart() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPreOpen() {
                mView!!.expandEnemiesArrow.setImageResource(R.drawable.ic_keyboard_arrow_up)
            }

            override fun onClosed() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPreClose() {
                mView!!.expandEnemiesArrow.setImageResource(R.drawable.ic_keyboard_arrow_down)
            }

        })

        //Process
        mView!!.expandProcess.setListener(object : ExpandableLayoutListener {
            override fun onAnimationEnd() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onOpened() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

            }

            override fun onAnimationStart() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPreOpen() {
                mView!!.expandProcessArrow.setImageResource(R.drawable.ic_keyboard_arrow_up)
            }

            override fun onClosed() {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPreClose() {
                mView!!.expandProcessArrow.setImageResource(R.drawable.ic_keyboard_arrow_down)
            }

        })


        //Expand onClickListener
        //Details
        mView!!.expandDetailsHeader.setOnClickListener {
            mView!!.expandDetails.toggle()
            //mView!!.expandDetails.collapse()
            //mView!!.expandDetails.expand()
        }

        mView!!.expandEnemiesHeader.setOnClickListener {
            mView!!.expandEnemies.toggle()
            //mView!!.expandDetails.collapse()
            //mView!!.expandDetails.expand()
        }

        mView!!.expandProcessHeader.setOnClickListener {
            mView!!.expandProcess.toggle()
            //mView!!.expandDetails.collapse()
            //mView!!.expandDetails.expand()
        }


    }


    private fun deleteCase(caseId: String) {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val deleteCaseReq =
            object : StringRequest(
                Method.POST,
                API.deleteCase,
                Response.Listener { response ->

                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context!!, "تم حذف القضية بنجاح")


                            //Remove Client Case Details
                            //Add Client Details

                            activity!!.supportFragmentManager
                                .beginTransaction()
                                .remove(
                                    activity!!.supportFragmentManager.findFragmentByTag(
                                        ClientsCasesDetailsFragment.TAG
                                    )!!
                                )
                                .replace(
                                    R.id.fragmentContainerMainActivity,
                                    ClientsDetailsFragment()
                                )
                                .commit()

                        }
                        "fail" -> {
                            KSnackToast().show(context!!, "لم يتم حذف القضية بنجاح")
                        }
                    }

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {


                override fun getParams(): MutableMap<String, String> {
                    val params2 = java.util.HashMap<String, String>()
                    params2["case_id"] = caseId
                    return params2
                }


            }

        mRequestQueue!!.add(deleteCaseReq)
    }

    /*private fun updateCase(newCase:CaseModel) {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val deleteCaseReq =
            object : StringRequest(
                Method.POST,
                API.updateCase,
                Response.Listener { response ->

                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(mContext, "تم حذف القضية بنجاح")

                            //TODO (update in local array)

                            notifyDataSetChanged() //refresh recycler View


                        }
                        "fail" -> {
                            KSnackToast().show(mContext, "لم يتم حذف القضية بنجاح")
                        }
                    }

                },
                Response.ErrorListener { error ->
                    KSnackToast().show(mContext, error.toString())
                }) {


                override fun getParams(): MutableMap<String, String> {
                    val params2 = java.util.HashMap<String, String>()
                    params2["case_id"] = newCase.id!!
                    params2["court_id"] = newCase.court_id!!
                    params2["type_id"] = newCase.type_id!!
                    params2["other_type"] = newCase.other_type!!
                    params2["internal_num"] = newCase.internal_num!!
                    params2["case_num"] = newCase.case_num!!
                    params2["account_num"] = newCase.account_num!!
                    params2["judge"] = newCase.judge!!
                    params2["case_type"] = newCase.case_type!!
                    params2["case_value"] = newCase.case_value!!
                    params2["date"] = newCase.date!!
                    params2["decision"] = newCase.decision!!
                    params2["status"] = newCase.status!!
                    return params2
                }


            }

        mRequestQueue!!.add(deleteCaseReq)
    }*/


}
