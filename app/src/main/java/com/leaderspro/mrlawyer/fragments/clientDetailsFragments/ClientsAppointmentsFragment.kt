package com.leaderspro.mrlawyer.fragments.clientDetailsFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.adapters.ClientAppointmentsAdapter
import com.leaderspro.mrlawyer.adapters.ClientsAdapter
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.AppointmentModel
import com.leaderspro.mrlawyer.models.ClientModelData
import kotlinx.android.synthetic.main.fragment_clients_appointment.view.*
import org.json.JSONArray
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

/**
 * A simple [Fragment] subclass.
 */
class ClientsAppointmentsFragment :
    Fragment() {

    companion object {
        const val TAG = "ClientsAppointmentsFragment"
    }

    var mView: View? = null

    private val clientAppointmentsList = ArrayList<AppointmentModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_clients_appointment, container, false)


        mView!!.clientAppointmentRecycler!!.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)


        //Client Name
        val strConcat = "الموكل : ${ClientsAdapter.sharedSelectedClient!!.fname}"
        mView!!.tvClientName.text = strConcat


        //hide add button when scroll down
        mView!!.clientAppointmentRecycler!!.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    mView!!.fabAddAppointment.hide()
                } else {
                    mView!!.fabAddAppointment.show()
                }
            }
        })



        mView!!.fabAddAppointment.setOnClickListener {
            //add insert client Fragment to container


            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()


            val fragmentContainerClientsDetails = R.id.fragmentContainerClientsDetails
            val insertClientAppointmentFragment =
                InsertClientAppointmentFragment()


            fragmentTransaction.replace(
                fragmentContainerClientsDetails,
                insertClientAppointmentFragment,
                InsertClientAppointmentFragment.TAG
            )
            fragmentTransaction.addToBackStack(InsertClientAppointmentFragment.TAG)
            fragmentTransaction.commit()

        }


        //client Appointments request
        clientAppointmentsRequest(ClientsAdapter.sharedSelectedClient!!.id!!)


        return mView

    }

    private fun clientAppointmentsRequest(clientId: String) {


        clientAppointmentsList.clear()

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(context!!)


        val clientAppointmentsReq =
            object :
                StringRequest(
                    Method.POST,
                    API.selectClientAppointments,
                    Response.Listener { response ->

                        val mainArray = JSONArray(response)
                        val jsonObject = mainArray.getJSONObject(0)

                        when (jsonObject.getString("message")) { // there is only one object
                            "empty" -> {
                                KSnackToast().show(
                                    context!!,
                                    "لا يوجد مواعيد لهذا الموكل"
                                )
                            }
                            "success" -> {

                                val dataArr = jsonObject.getJSONArray("data")

                                for (x in 0 until dataArr.length()) {
                                    val clientName = dataArr.getJSONObject(x).get("clientName")
                                    val advName = dataArr.getJSONObject(x).get("advName")
                                    val court = dataArr.getJSONObject(x).get("court")
                                    val timeAndDate =
                                        dataArr.getJSONObject(x).get("timeAndDate")
                                    val notes = dataArr.getJSONObject(x).get("notes")



                                    clientAppointmentsList.add(
                                        AppointmentModel(
                                            "$clientName",
                                            "$advName",
                                            "$court",
                                            "$timeAndDate",
                                            "<الخصوم>",
                                            "<المحكمة>",
                                            "$notes"
                                        )
                                    )
                                }//end of for loop (dataArr)

                                // appointment Adapter layout
                                mView!!.clientAppointmentRecycler.adapter =
                                    ClientAppointmentsAdapter(context!!, clientAppointmentsList)


                            }
                            else -> {
                                //something else error
                                KSnackToast().show(context!!, response.toString())

                            }
                        }

                    },
                    Response.ErrorListener { error ->
                        KSnackToast().show(context!!, error.toString())
                    }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["clientId"] = clientId
                    return params2
                }

            }

        mRequestQueue!!.add(clientAppointmentsReq)
    }


    private fun insertClientAppointmentsRequest(clientId: String) {


        clientAppointmentsList.clear()

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(context!!)


        val clientAppointmentsReq =
            object :
                StringRequest(
                    Method.POST,
                    API.selectClientAppointments,
                    Response.Listener { response ->

                        val mainArray = JSONArray(response)
                        val jsonObject = mainArray.getJSONObject(0)

                        when (jsonObject.getString("message")) { // there is only one object
                            "empty" -> {
                                KSnackToast().show(
                                    context!!,
                                    "لا يوجد مواعيد لهذا الموكل"
                                )
                            }
                            "success" -> {

                                val dataArr = jsonObject.getJSONArray("data")

                                for (x in 0 until dataArr.length()) {
                                    val clientName = dataArr.getJSONObject(x).get("clientName")
                                    val advName = dataArr.getJSONObject(x).get("advName")
                                    val court = dataArr.getJSONObject(x).get("court")
                                    val timeAndDate =
                                        dataArr.getJSONObject(x).get("timeAndDate")
                                    val notes = dataArr.getJSONObject(x).get("notes")



                                    clientAppointmentsList.add(
                                        AppointmentModel(
                                            "$clientName",
                                            "$advName",
                                            "$court",
                                            "$timeAndDate",
                                            "<الخصوم>",
                                            "<المحكمة>",
                                            "$notes"
                                        )
                                    )
                                }//end of for loop (dataArr)

                                // appointment Adapter layout
                                mView!!.clientAppointmentRecycler.adapter =
                                    ClientAppointmentsAdapter(context!!, clientAppointmentsList)


                            }
                            else -> {
                                //something else error
                                KSnackToast().show(context!!, response.toString())

                            }
                        }

                    },
                    Response.ErrorListener { error ->
                        KSnackToast().show(context!!, error.toString())
                    }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["clientId"] = clientId
                    return params2
                }

            }

        mRequestQueue!!.add(clientAppointmentsReq)
    }


}
