package com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.CaseEnemyModel
import kotlinx.android.synthetic.main.fragment_insert_client_case_enemies.*
import kotlinx.android.synthetic.main.fragment_insert_client_case_enemies.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class InsertClientCaseEnemiesFragment(val caseId: String) : Fragment() {

    companion object {
        const val TAG = "InsertClientCaseEnemiesFragment"
    }

    var mView: View? = null

    private var spinnerEnemyDescriptionAdapter: ArrayAdapter<String>? = null
    val enemyDescriptionStringArr = ArrayList<String>()
    val enemyDescriptionIdArr = ArrayList<String>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_insert_client_case_enemies, container, false)


        //Lawyer Spinner
        spinnerEnemyDescriptionAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, enemyDescriptionStringArr)
        mView!!.spinnerEnemyDescription.setPositiveButton("رجوع")

        enemyDescReq()

        mView!!.btnAddCaseEnemy.setOnClickListener {

            val newEnemy = CaseEnemyModel()


            if (checkUserInput()) {

                newEnemy.e_name = mView!!.etCaseEnemyName.text.toString()
                newEnemy.e_sid = mView!!.etCaseEnemySID.text.toString()
                newEnemy.e_phone = mView!!.etCaseEnemyPhone.text.toString()
                newEnemy.e_address = mView!!.etCaseEnemyAddress.text.toString()

                newEnemy.description_id =
                    enemyDescriptionIdArr[spinnerEnemyDescription.selectedItemPosition]

                newEnemy.adv_name = mView!!.etCaseEnemyAdv.text.toString()
                newEnemy.adv_phone = mView!!.etCaseEnemyAdvPhone.text.toString()
                newEnemy.adv_address = mView!!.etCaseEnemyAdvAddress.text.toString()

                insertEnemyReq(newEnemy)
            }


        }



        return mView
    }

    //load Enemy Description in Spinner
    private fun enemyDescReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val enemyDescriptionReq =
            object :
                StringRequest(
                    Method.POST,
                    API.selectEnemyDescription,
                    Response.Listener { response ->

                        val mainArray = JSONArray(response)

                        for (i in 0 until mainArray.length()) {
                            when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                                "Case Description" -> {
                                    when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                        "empty" -> {
                                            //TODO("If enemy desc is empty ")
                                        }

                                        "success" -> { //fill card array list

                                            val mData =
                                                mainArray.getJSONObject(i).getJSONArray("data")
                                            for (x in 0 until mData.length()) {

                                                enemyDescriptionIdArr.add(
                                                    mData.getJSONObject(x).getString("id")
                                                )
                                                enemyDescriptionStringArr.add(
                                                    mData.getJSONObject(x).getString("description")
                                                )

                                            }//end of data loop


                                            // client Adapter layout
                                            mView!!.spinnerEnemyDescription.adapter =
                                                spinnerEnemyDescriptionAdapter


                                        }//end of success case
                                    }//end of message(when)
                                }//end of clients case


                            }


                        }//end of for-loop (mainArray)

                    },
                    Response.ErrorListener { error ->
                        KSnackToast().show(context!!, error.toString())
                    }) {


            }

        mRequestQueue!!.add(enemyDescriptionReq)
    }


    private fun checkUserInput(): Boolean {

        if (mView!!.etCaseEnemyName.text.toString() == "") {
            mView!!.etCaseEnemyName.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد إسم الخصم")
            return false
        }


        if (mView!!.spinnerEnemyDescription.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد صفة الخصم")
            return false
        }



        return true

    }


    private fun insertEnemyReq(newEnemy: CaseEnemyModel) {


        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(context)

        //login Request initialized
        val insertEnemyReq =
            object :
                StringRequest(Method.POST, API.insertEnemy, Response.Listener { response ->

                    //check if successful insert Then close the fragment
                    val obj = JSONObject(response)

                    when (obj.getString("message")) {

                        "success" -> {
                            KSnackToast().show(context!!, "تم إضافة الخصم بنجاح")

                            //TODO(add enemy to recyclerView)

                        }
                        "fail" -> {
                            KSnackToast().show(context!!, "لم يتم إضافة الخصم بنجاح")
                        }
                    }

                }, Response.ErrorListener { error ->
                    KSnackToast().show(context!!, error.toString())
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["case_id"] = caseId
                    params2["e_name"] = newEnemy.e_name!!
                    params2["e_sid"] = newEnemy.e_sid!!
                    params2["e_phone"] = newEnemy.e_phone!!
                    params2["e_address"] = newEnemy.e_address!!
                    params2["adv_name"] = newEnemy.adv_name!!
                    params2["adv_phone"] = newEnemy.adv_phone!!
                    params2["adv_address"] = newEnemy.adv_address!!
                    params2["description_id"] = newEnemy.description_id!!


                    return params2
                }

            }
        mRequestQueue!!.add(insertEnemyReq)

    }


}
