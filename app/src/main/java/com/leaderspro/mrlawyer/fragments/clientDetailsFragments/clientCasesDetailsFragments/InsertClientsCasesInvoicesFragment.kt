package com.leaderspro.mrlawyer.fragments.clientDetailsFragments.clientCasesDetailsFragments


import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.ConstantValue
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.InvoiceModel
import kotlinx.android.synthetic.main.fragment_insert_case_invoices.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class InsertClientsCasesInvoicesFragment(val caseId: String) : Fragment() {

    companion object {
        const val TAG = "InsertClientCaseEnemiesFragment"
    }

    var mView: View? = null

    var cal: Calendar? = null


    private var spinnerInvoiceInsteadOfAdapter: ArrayAdapter<String>? = null
    val invoiceInsteadOfStringArr = ArrayList<String>()
    val invoiceInsteadOfIdArr = ArrayList<String>()

    private var spinnerPaymentMethodAdapter: ArrayAdapter<String>? = null
    val paymentMethodStringArr = ArrayList<String>()

    private var spinnerInvoiceStatusAdapter: ArrayAdapter<String>? = null
    val invoiceStatusStringArr = ArrayList<String>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_insert_case_invoices, container, false)


        //Instead ofSpinner
        spinnerInvoiceInsteadOfAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, invoiceInsteadOfStringArr)
        mView!!.spinnerInvoiceInsteadOf.setPositiveButton("رجوع")

        invoicesInsteadOfTypeReq()


        paymentMethodStringArr.add("نقدا")
        paymentMethodStringArr.add("شيك")
        spinnerPaymentMethodAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, paymentMethodStringArr)
        mView!!.spinnerPaymentMethod.setPositiveButton("رجوع")
        mView!!.spinnerPaymentMethod.adapter = spinnerPaymentMethodAdapter



        invoiceStatusStringArr.add("مدفوع")
        invoiceStatusStringArr.add("غير مدفوع")
        spinnerInvoiceStatusAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, invoiceStatusStringArr)
        mView!!.spinnerInvoiceStatus.setPositiveButton("رجوع")

        mView!!.spinnerInvoiceStatus.adapter = spinnerInvoiceStatusAdapter


        //contract date button
        cal = Calendar.getInstance()
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal!!.set(Calendar.YEAR, year)
                cal!!.set(Calendar.MONTH, monthOfYear)
                cal!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat(ConstantValue.dataFormat, Locale.US)

                val strDate = sdf.format(cal!!.time)
                mView!!.tvChequeDate.text = strDate

            }

        mView!!.ivChequeDate.setOnClickListener {
            DatePickerDialog(
                container!!.context,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal!!.get(Calendar.YEAR),
                cal!!.get(Calendar.MONTH),
                cal!!.get(Calendar.DAY_OF_MONTH)
            ).show()
        }





        mView!!.btnAddInvoices.setOnClickListener {

            if (checkUserInput()) {
                val mInvoice = InvoiceModel()

                mInvoice.cost = mView!!.etInvoicesCost.text.toString()
                mInvoice.instead_of =
                    invoiceInsteadOfIdArr[mView!!.spinnerInvoiceInsteadOf.selectedItemPosition]

                mInvoice.note = mView!!.etInvoiceNote.text.toString()
                mInvoice.method = "${mView!!.spinnerPaymentMethod.selectedItemPosition + 1}"
                mInvoice.cheque_date = mView!!.tvChequeDate.text.toString()
                mInvoice.cheque_num = mView!!.etInvoiceChequeNumber.text.toString()
                mInvoice.is_paid = "${mView!!.spinnerInvoiceStatus.selectedItemPosition + 1}"




                insertInvoice(mInvoice)
            }
        }



        return mView
    }

    //load instead of type in Spinner
    private fun invoicesInsteadOfTypeReq() {

        val mRequestQueue = Volley.newRequestQueue(activity)

        val insteadOfTypeReq =
            object :
                StringRequest(
                    Method.POST,
                    API.selectCaseInvoicesInsteadOfType,
                    Response.Listener { response ->

                        val mainArray = JSONArray(response)

                        for (i in 0 until mainArray.length()) {
                            when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                                "Invoice Instead Of Type" -> {
                                    when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                        "empty" -> {
                                            //TODO("If enemy desc is empty ")
                                        }

                                        "success" -> { //fill card array list

                                            val mData =
                                                mainArray.getJSONObject(i).getJSONArray("data")
                                            for (x in 0 until mData.length()) {

                                                invoiceInsteadOfIdArr.add(
                                                    mData.getJSONObject(x).getString("id")
                                                )
                                                invoiceInsteadOfStringArr.add(
                                                    mData.getJSONObject(x).getString("name")
                                                )

                                            }//end of data loop


                                            // client Adapter layout
                                            mView!!.spinnerInvoiceInsteadOf.adapter =
                                                spinnerInvoiceInsteadOfAdapter


                                        }//end of success case
                                    }//end of message(when)
                                }//end of clients case


                            }


                        }//end of for-loop (mainArray)

                    },
                    Response.ErrorListener { error ->
                        KSnackToast().show(context!!, error.toString())
                    }) {


            }

        mRequestQueue!!.add(insteadOfTypeReq)
    }


    private fun checkUserInput(): Boolean {

        if (mView!!.etInvoicesCost.text.toString() == "") {
            mView!!.etInvoicesCost.error = ""
            KSnackToast().show(context!!, "الرجاء تحديد المبلغ")
            return false
        }


        if (mView!!.spinnerInvoiceInsteadOf.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد بدل المبلغ")
            return false
        }


        if (mView!!.spinnerPaymentMethod.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد طريقة الدفع")
            return false
        }


        if (mView!!.tvChequeDate.text == getString(R.string.emptyDate)) {
            KSnackToast().show(context!!, "الرجاء تحديد تاريخ الشيك")
            return false
        }



        if (mView!!.spinnerInvoiceStatus.selectedItemPosition == -1) {
            KSnackToast().show(context!!, "الرجاء تحديد حالة الفاتورة")
            return false
        }


        return true

    }


    private fun insertInvoice(invoiceModel: InvoiceModel) {


        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(context)

        //login Request initialized
        val insertInvoiceReq =
            object :
                StringRequest(
                    Method.POST,
                    API.insertClientCaseInvoice,
                    Response.Listener { response ->

                        //check if successful insert Then close the fragment
                        val obj = JSONObject(response)

                        when (obj.getString("message")) {

                            "success" -> {
                                KSnackToast().show(context!!, "تم إضافة الدفعة بنجاح")

                                //TODO(add enemy to recyclerView)

                            }
                            "fail" -> {
                                KSnackToast().show(context!!, "لم يتم إضافة الدفعة بنجاح")
                            }
                        }

                    },
                    Response.ErrorListener { error ->
                        KSnackToast().show(context!!, error.toString())
                    }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["case_id"] = caseId
                    params2["cost"] = invoiceModel.cost!!
                    params2["method"] = invoiceModel.method!!
                    params2["cheque_date"] = invoiceModel.cheque_date!!
                    //params2["date"] = invoiceModel.date!!
                    params2["note"] = invoiceModel.note!!
                    params2["instead_of"] = invoiceModel.instead_of!!
                    //params2["file"] =
                    params2["is_paid"] = invoiceModel.is_paid!!


                    return params2
                }

            }
        mRequestQueue!!.add(insertInvoiceReq)

    }


}
