package com.leaderspro.mrlawyer.fragments


import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.helper.ConstantValue
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.helper.SharedPreference
import com.leaderspro.mrlawyer.models.UserModel
import kotlinx.android.synthetic.main.fragment_insert_todo.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class InsertTodoFragment : Fragment() {

    companion object {
        const val TAG = "InsertTodoFragment"
    }

    var mView: View? = null
    var cal: Calendar? = null
    var mDate: String? = null

    private var todoTask: EditText? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_insert_todo, container, false)

        //don't remove this!
        val insertTodoHeader = mView!!.insertTodoHeader
        insertTodoHeader.setOnClickListener {}


        val ivCalender = mView!!.ivCalender
        todoTask = mView!!.todoTask


        cal = Calendar.getInstance()


        // create an OnDateSetListener
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal!!.set(Calendar.YEAR, year)
                cal!!.set(Calendar.MONTH, monthOfYear)
                cal!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat(ConstantValue.dataFormat, Locale.US)
                mDate = sdf.format(cal!!.time)
                mView!!.todoDateReview.text = mDate


            }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener

        ivCalender.setOnClickListener {
            DatePickerDialog(
                container!!.context,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal!!.get(Calendar.YEAR),
                cal!!.get(Calendar.MONTH),
                cal!!.get(Calendar.DAY_OF_MONTH)
            ).show()
        }


        mView!!.btnInsertTodo.setOnClickListener {

            //userId - task  - date

            val id = SharedPreference(activity!!, UserModel.sharedFileKey).readSharedLogin()
                .id.toString()


            //check task
            val t: String?
            if (todoTask!!.text.toString() == "") {//task is empty !
                KSnackToast().show(context!!,getString(R.string.pleaseWriteYourTask))
                return@setOnClickListener
            } else {
                t = todoTask!!.text.toString().trim()
            }

            //check date
            if (mDate == null) {//date not selected !
                KSnackToast().show(context!!,getString(R.string.pleaseSelectDate))
                return@setOnClickListener
            }

            insertTodoReq(id, t, mDate!!)
        }


        return mView
    }


    private fun insertTodoReq(userId: String, task: String, date: String) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(activity)

        //login Request initialized
        val insertTodo =
            object :
                StringRequest(Method.POST, API.insertTodo, Response.Listener { response ->

                    //check if successful insert Then close the fragment
                    val obj = JSONObject(response)

                    when(obj.getString("message")){

                        "success" ->{
                            KSnackToast().show(context!!,getString(R.string.taskAddedSuccessfully))
                            activity!!.supportFragmentManager.beginTransaction().remove(this).commit()

                            //TODO(add task to recyclerView)

                        }
                        "fail" ->{
                            KSnackToast().show(context!!,getString(R.string.taskAddedUnsuccessfully))
                        }
                    }

                }, Response.ErrorListener { error ->
                    KSnackToast().show(context!!,error.toString())
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["user_id"] = userId
                    params2["task"] = task
                    params2["date"] = date
                    return params2
                }

            }
        mRequestQueue!!.add(insertTodo)

    }




}
