package com.leaderspro.mrlawyer.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.API
import com.leaderspro.mrlawyer.R
import com.leaderspro.mrlawyer.adapters.ClientsAdapter
import com.leaderspro.mrlawyer.fragments.clientDetailsFragments.InsertClientFragment
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.models.ClientModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.android.synthetic.main.fragment_clients.view.*
import org.json.JSONArray

/**
 * A simple [Fragment] subclass.
 */
class ClientsFragment : Fragment() {

    companion object {
        const val TAG = "ClientsFragment"
    }

    var mView: View? = null

    val clientsList = ArrayList<ClientModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_clients, container, false)



        mView!!.clientRecycler!!.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)


        mView!!.fabAddClient.setOnClickListener {
            //add insert client Fragment to container


            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()


            val fragmentContainerClients = R.id.fragmentContainerMainActivity
            val insertClientFragment = InsertClientFragment()


            fragmentTransaction.add(
                fragmentContainerClients,
                insertClientFragment,
                InsertClientFragment.TAG
            )
            fragmentTransaction.addToBackStack(InsertClientFragment.TAG)
            fragmentTransaction.commit()

        }


        //hide add button when scroll down
        mView!!.clientRecycler!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    mView!!.fabAddClient.hide()
                } else {
                    mView!!.fabAddClient.show()
                }
            }
        })


        //clients request
        clientsRequest()





        return mView
    }


    private fun clientsRequest() {
        clientsList.clear()

        val mRequestQueue = Volley.newRequestQueue(activity)

        val homeReq =
            object : StringRequest(Method.GET, API.selectClients, Response.Listener { response ->

                val mainArray = JSONArray(response)

                for (i in 0 until mainArray.length()) {
                    when (mainArray.getJSONObject(i).getString("title")) {//check title appointment - caseCount

                        "Clients" -> {
                            when (mainArray.getJSONObject(i).getString("message")) {//check if message success
                                "empty" -> {
                                    //TODO("If clients is empty ")
                                }

                                "success" -> { //fill card array list

                                    //Moshi Library
                                    val b = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                                    val adp =
                                        b.adapter<ClientModel>(ClientModel::class.java)
                                    val e =
                                        adp.fromJson(mainArray.getJSONObject(i).toString()) //model

                                    clientsList.add(e!!)


                                    // client Adapter layout
                                    mView!!.clientRecycler.adapter =
                                        ClientsAdapter(clientsList, context!!)

                                    mView!!.clientRecycler.adapter!!.notifyDataSetChanged()


                                }//end of success case
                            }//end of message(when)
                        }//end of clients case


                    }


                }//end of for-loop (mainArray)

            }, Response.ErrorListener { error ->
                KSnackToast().show(context!!,error.toString())
            }) {


            }

        mRequestQueue!!.add(homeReq)
    }



}
