package com.leaderspro.mrlawyer.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.leaderspro.mrlawyer.R

/**
 * A simple [Fragment] subclass.
 */
class ContractsAndModelsFragment : Fragment() {

    companion object {
        const val TAG = "ContractsAndModelsFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contracts_and_models, container, false)
    }


}
