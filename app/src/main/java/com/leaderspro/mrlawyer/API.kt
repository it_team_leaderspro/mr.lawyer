package com.leaderspro.mrlawyer

class API {

    companion object {
        const val weather =
            "http://api.openweathermap.org/data/2.5/forecast?id=250441&APPID=6e9ef3d1330677e5e997c46fc1d76a0f"

        private const val main = "https://www.test.leaders-pro.net/api"

        const val login = "$main/login.php"

        const val home = "$main/home.php"

        //ToDo
        const val insertTodo = "$main/insert_todo.php"
        const val updateTodo = "$main/update_todo.php"

        //Clients
        const val insertClient = "$main/insert_client.php"
        const val updateClient = "$main/update_client.php"
        const val selectClients = "$main/select_clients.php"
        const val selectClientAppointments = "$main/select_client_appointments.php"

        const val selectCourts = "$main/select_courts.php"
        const val selectClientCaseNumber = "$main/select_client_case_number.php"

        const val selectAppointmentsType = "$main/select_appointments_type.php"
        const val insertClientAppointment = "$main/insert_client_appointment.php"


        const val insertCase = "$main/insert_case.php"
        const val updateCase = "$main/update_case.php"
        const val deleteCase = "$main/delete_case.php"

        //case cost
        const val selectCaseCost = "$main/select_case_cost.php"
        const val insertCaseCost = "$main/insert_case_cost.php"
        const val updateCaseCost = "$main/update_case_cost.php"


        //enemy
        const val selectEnemyDescription = "$main/select_enemy_description.php"
        const val insertEnemy = "$main/insert_case_enemies.php"
        const val deleteEnemy = "$main/delete_case_enemies.php"
        const val selectEnemies = "$main/select_case_enemies.php"

        //process
        const val selectProcess = "$main/select_case_process.php"
        const val insertProcess = "$main/insert_case_process.php"
        const val deleteProcess = "$main/delete_case_process.php"


        const val selectCaseType = "$main/select_case_type.php"
        const val selectCaseStatus = "$main/select_case_status.php"


        const val selectCaseInvoicesInsteadOfType = "$main/select_invoices_instead_of_type.php"
        const val insertClientCaseInvoice = "$main/insert_client_case_invoice.php"


        const val selectAppointmentsCases = "$main/select_client_cases.php"

        const val selectLawyers = "$main/select_lawyers.php"


    }

}