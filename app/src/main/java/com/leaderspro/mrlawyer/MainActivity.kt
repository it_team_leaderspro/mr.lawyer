package com.leaderspro.mrlawyer


import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.developer.kalert.KAlertDialog
import com.leaderspro.mrlawyer.fragments.*
import com.leaderspro.mrlawyer.helper.SharedPreference
import com.leaderspro.mrlawyer.models.UserModel
import com.shrikanthravi.customnavigationdrawer2.data.MenuItem
import com.shrikanthravi.customnavigationdrawer2.widget.SNavigationDrawer.logout
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val menuItemList = ArrayList<MenuItem>()

    //Fragments
    private val fragmentContainerMainActivity = R.id.fragmentContainerMainActivity
    private val homeFragment = HomeFragment()
    private val appointmentsFragment = AppointmentsFragment()
    private val clientsFragment = ClientsFragment()
    private val casesAndServicesFragment = CasesAndServicesFragment()
    private val contractsAndModelsFragment = ContractsAndModelsFragment()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        menuItemList.add(MenuItem(getString(R.string.NavHome), R.color.colorAccent))
        menuItemList.add(MenuItem(getString(R.string.NavAppointments), R.color.colorAccent))
        menuItemList.add(MenuItem(getString(R.string.NavClients), R.color.colorAccent))
        menuItemList.add(MenuItem(getString(R.string.NavCasesAndServices), R.color.colorAccent))
        menuItemList.add(MenuItem(getString(R.string.NavContractsAndModels), R.color.colorAccent))

        // the first time that the app open
        navigationDrawer.setAppbarTitleTV(getString(R.string.NavHome))
        navigationDrawer.menuItemList = menuItemList

        logout.setOnClickListener {
            KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.AreYouSure))
                .setContentText(getString(R.string.logoutMessage))
                .setConfirmText(getString(R.string.Yes))
                .setCancelText(getString(R.string.cancel))
                .setConfirmClickListener { sDialog ->
                    //clear shared preference
                    val sp =
                        SharedPreference(this@MainActivity, UserModel.sharedFileKey)
                    sp.clearSharedLogin()

                    //Exit to LoginActivity
                    startActivity(Intent(applicationContext, LoginActivity::class.java))
                    finish()

                    sDialog.dismissWithAnimation()
                }
                .setCancelClickListener { sDialog -> sDialog.cancel() }
                .show()


        }

        //Load Default Fragment (Home Fragment)
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(fragmentContainerMainActivity, homeFragment, HomeFragment.TAG)
        fragmentTransaction.commit()



        navigationDrawer.setOnMenuItemClickListener { position: Int ->
            when (position) {

                0 -> replaceFragment(homeFragment, false, HomeFragment.TAG)

                1 -> replaceFragment(appointmentsFragment, false, AppointmentsFragment.TAG)

                2 -> replaceFragment(clientsFragment, false, ClientsFragment.TAG)

                3 -> replaceFragment(casesAndServicesFragment,false,CasesAndServicesFragment.TAG)

                4 -> replaceFragment(contractsAndModelsFragment,false,ContractsAndModelsFragment.TAG)

            }

        }


    }


    private fun replaceFragment(fragment: Fragment, addToBack: Boolean, TAG: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(fragmentContainerMainActivity, fragment, TAG)
        if (addToBack)
            transaction.addToBackStack(TAG)
        transaction.commit()

    }

}
