package com.leaderspro.mrlawyer

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.helper.KSnackToast
import com.leaderspro.mrlawyer.helper.SharedPreference
import com.leaderspro.mrlawyer.models.UserModel
import com.onurkagan.ksnack_lib.Animations.Slide
import com.onurkagan.ksnack_lib.KSnack.KSnack
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONArray
import java.util.*


class LoginActivity : AppCompatActivity() {

    val TAG = "LoginActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)



        btnLogin.setOnClickListener {
            val userName: String?
            val password: String?

            //check user input
            if (etUserName.text!!.isNotEmpty() && etPassword.text!!.isNotEmpty()) {
                userName = etUserName.text.toString()
                password = etPassword.text.toString()
            } else {
                return@setOnClickListener //exit from "OnClickListener"
            }

            login(userName, password)

        }
    }


    private fun login(userName: String, password: String) {

        //RequestQueue initialized
        val mRequestQueue = Volley.newRequestQueue(this)

        //login Request initialized
        val loginReq =
            object :
                StringRequest(Method.POST, API.login, Response.Listener { response ->

                    val mainArray = JSONArray(response)
                    val jsonObject = mainArray.getJSONObject(0)

                    when (jsonObject.getString("message")) { // there is only one object
                        "empty" -> {
                            //username and password not found in DB
                            KSnackToast().show(this, getString(R.string.checkUsernameAndPassword))
                        }
                        "success" -> {
                            //Login successfully
                            if (chKeepLoggedIn.isChecked) {
                                val sp =
                                    SharedPreference(this@LoginActivity, UserModel.sharedFileKey)
                                val user = UserModel()
                                //get the first index in "data" from first object

                                //check if user checked keep loggedIn
                                val data = jsonObject.getJSONArray("data").getJSONObject(0)
                                user.id = data.getString(UserModel.COLUMN_ID)
                                user.fName = data.getString(UserModel.COLUMN_fName)
                                user.lName = data.getString(UserModel.COLUMN_lName)
                                user.email = data.getString(UserModel.COLUMN_email)
                                user.phone = data.getString(UserModel.COLUMN_phone)
                                user.userName = data.getString(UserModel.COLUMN_userName)
                                user.password = password
                                user.image = data.getString(UserModel.COLUMN_image)
                                user.role = data.getString(UserModel.COLUMN_role)
                                user.type = data.getString(UserModel.COLUMN_type)

                                //save user info as shared pref.
                                sp.writeSharedLogin(user)
                            }

                            startActivity(Intent(applicationContext, MainActivity::class.java))


                        }
                        else -> {
                            //something else error
                            KSnackToast().show(this, response.toString())

                        }
                    }

                }, Response.ErrorListener { error ->
                    KSnackToast().show(this, error.toString())
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params2 = HashMap<String, String>()
                    params2["username"] = userName
                    params2["password"] = password
                    return params2
                }

            }
        mRequestQueue!!.add(loginReq)
        loginReq.retryPolicy = object : RetryPolicy {
            override fun retry(error: VolleyError?) {

            }

            override fun getCurrentRetryCount(): Int {
                return 3
            }

            override fun getCurrentTimeout(): Int {
                return 10000
            }
        }



    }


}

