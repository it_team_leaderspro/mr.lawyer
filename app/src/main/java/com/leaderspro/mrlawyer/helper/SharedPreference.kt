package com.leaderspro.mrlawyer.helper

import android.app.Activity
import android.content.Context
import com.leaderspro.mrlawyer.models.UserModel

class SharedPreference(private val activity: Activity, private val fileKey: String) {

    private val sharedPreference =
        this.activity.getSharedPreferences(this.fileKey, Context.MODE_PRIVATE)


    //Write to shared preferences
    fun writeSharedLogin(
        userModel: UserModel
    ) {

        val editor = sharedPreference.edit()

        editor.putString(UserModel.COLUMN_ID, userModel.id!!)
        editor.putString(UserModel.COLUMN_fName, userModel.fName!!)
        editor.putString(UserModel.COLUMN_lName, userModel.lName!!)
        editor.putString(UserModel.COLUMN_email, userModel.email!!)
        editor.putString(UserModel.COLUMN_phone, userModel.phone!!)
        editor.putString(UserModel.COLUMN_userName, userModel.userName!!)
        editor.putString(UserModel.COLUMN_password, userModel.password!!)
        editor.putString(UserModel.COLUMN_image, userModel.image!!)
        editor.putString(UserModel.COLUMN_role, userModel.role!!)
        editor.putString(UserModel.COLUMN_type, userModel.type!!)
        editor.apply()

    }


    //Read from shared preferences
    fun readSharedLogin(): UserModel {
        val user = UserModel()

        user.id = sharedPreference.getString(UserModel.COLUMN_ID, null)
        user.fName = sharedPreference.getString(UserModel.COLUMN_fName, null)
        user.lName = sharedPreference.getString(UserModel.COLUMN_lName, null)
        user.email = sharedPreference.getString(UserModel.COLUMN_email, null)
        user.phone = sharedPreference.getString(UserModel.COLUMN_phone, null)
        user.userName = sharedPreference.getString(UserModel.COLUMN_userName, null)
        user.password = sharedPreference.getString(UserModel.COLUMN_password, null)
        user.image = sharedPreference.getString(UserModel.COLUMN_image, null)
        user.role = sharedPreference.getString(UserModel.COLUMN_role, null)
        user.type = sharedPreference.getString(UserModel.COLUMN_type, null)

        return user

    }

    fun clearSharedLogin(){
        val editor = sharedPreference.edit()
        editor.clear()
        editor.apply()
    }



}