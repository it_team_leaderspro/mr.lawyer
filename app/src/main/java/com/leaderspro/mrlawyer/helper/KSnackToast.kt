package com.leaderspro.mrlawyer.helper

import android.app.Activity
import android.content.Context
import com.onurkagan.ksnack_lib.Animations.Slide
import com.onurkagan.ksnack_lib.KSnack.KSnack

class KSnackToast {


    fun show(context: Context, message: String) {
        val kSnack = KSnack(context as Activity)
            .setMessage(message) // message
            .setTextColor(android.R.color.white) // message text color
            .setAnimation(
                Slide.Up.getAnimation(KSnack(context).getSnackView()),
                Slide.Down.getAnimation(KSnack(context).getSnackView())
            )
            .setDuration(2000) // you can use for auto close.
            .show()
    }
}