package com.leaderspro.mrlawyer

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.leaderspro.mrlawyer.helper.SharedPreference
import com.leaderspro.mrlawyer.models.UserModel
import com.onurkagan.ksnack_lib.Animations.Slide
import com.onurkagan.ksnack_lib.KSnack.KSnack
import org.json.JSONArray

class SplashActivity : AppCompatActivity() {

    var user = UserModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            login()
        }, 2000)



    }

    private fun login() {
        //get saved user Info
        val sp = SharedPreference(this, UserModel.sharedFileKey)
        user = sp.readSharedLogin()
        val userName: String? = user.userName
        val password: String? = user.password

        if (userName == null && password == null) {
            //user info not saved in shared pref.
            //go to login activity
            startActivity(Intent(applicationContext, LoginActivity::class.java))

        } else {
            //request login api

            //RequestQueue initialized
            val mRequestQueue = Volley.newRequestQueue(this@SplashActivity)

            //login Request
            val loginReq =
                object : StringRequest(
                    Method.POST,
                    API.login,
                    Response.Listener { response ->

                        val mainArray = JSONArray(response)
                        val jsonObject = mainArray.getJSONObject(0)

                        when (jsonObject.getString("message")) { // there is only one object
                            "empty" -> {
                                //username and password not found in DB
                                val kSnack = KSnack(this@SplashActivity)
                                    .setMessage(getString(R.string.checkUsernameAndPassword)) // message
                                    .setTextColor(android.R.color.white) // message text color
                                    .setAnimation(
                                        Slide.Up.getAnimation(KSnack(this@SplashActivity).getSnackView()),
                                        Slide.Down.getAnimation(KSnack(this@SplashActivity).getSnackView())
                                    )
                                    .setDuration(2000) // you can use for auto close.
                                    .show()


                                //clear shared pref.
                                sp.clearSharedLogin()

                                //go to LoginActivity
                                Handler().postDelayed({
                                    startActivity(
                                        Intent(
                                            applicationContext,
                                            LoginActivity::class.java
                                        )
                                    )
                                }, 2500)
                            }

                            "success" -> {
                                //Login Successfully
                                Handler().postDelayed({
                                    startActivity(
                                        Intent(
                                            applicationContext,
                                            MainActivity::class.java
                                        )
                                    )
                                }, 2500)

                            }
                            else -> {
                                //something else error
                                Toast.makeText(
                                    applicationContext,
                                    response.toString(),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }

                    },
                    Response.ErrorListener { error ->
                        Toast.makeText(
                            applicationContext,
                            error.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                    }) {

                    override fun getParams(): MutableMap<String?, String?> {
                        val params2 = HashMap<String?, String?>()
                        params2["username"] = userName
                        params2["password"] = password
                        return params2
                    }

                }
            mRequestQueue!!.add(loginReq)
            loginReq.retryPolicy = object : RetryPolicy {
                override fun retry(error: VolleyError?) {
                    //
                }

                override fun getCurrentRetryCount(): Int {
                    return 3
                }

                override fun getCurrentTimeout(): Int {
                    return 10000
                }
            }
        }


    }

}
