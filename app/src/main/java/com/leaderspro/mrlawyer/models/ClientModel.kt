package com.leaderspro.mrlawyer.models


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ClientModel(
    val `data`: List<ClientModelData?>?,
    val message: String?,
    val title: String?
)

@JsonClass(generateAdapter = true)
data class ClientModelData(
    val address: String?,
    val advFName: String?,
    @Json(name = "adv_id")
    val advId: String?,
    val advLName: String?,
    @Json(name = "contract_date")
    val contractDate: String?,
    val email: String?,
    val fname: String?,
    val id: String?,
    val image: String?,
    val lname: String?,
    val password: String?,
    val phone: String?,
    val sid: String?,
    val type: String?,
    @Json(name = "yearly_cost")
    val yearlyCost: String?
)