package com.leaderspro.mrlawyer.models

class CaseCostModel(
    var id: String? = null,
    var case_id: String? = null,
    var cost: String? = null,
    var fees: String? = null,
    var execution_fees: String? = null,
    var execution_expenses: String? = null,
    var ads: String? = null,
    var spending: String? = null,
    var unique_fees: String? = null,
    var resume_fees: String? = null,
    var experience_fees: String? = null,
    var auction_fees: String? = null,
    var stamps: String? = null,
    var other: String? = null,
    var offer: String? = null

)