package com.leaderspro.mrlawyer.models


data class AppointmentModel(

    val clientName: String,
    val advName: String,
    val appointmentType: String,
    val timeAndDate: String,
    val enemies: String,
    val court: String,
    val notes: String
)
