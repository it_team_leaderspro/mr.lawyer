package com.leaderspro.mrlawyer.models

class InvoiceModel(

    var id: String? = null,
    var case_id: String? = null,
    var invoice_num: String? = null,
    var cost: String? = null,
    var method: String? = null,
    var cheque_num: String? = null,
    var cheque_date: String? = null,
    var date: String? = null,
    var note: String? = null,
    var instead_of: String? = null,
    var file: String? = null,
    var is_paid: String? = null

)
