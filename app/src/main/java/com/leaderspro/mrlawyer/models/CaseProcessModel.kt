package com.leaderspro.mrlawyer.models

class CaseProcessModel(
    var id: String? = null,
    var case_id: String? = null,
    var process: String? = null,
    var is_next: String? = null,
    var date: String? = null,
    var register_date: String? = null,
    var status: String? = null,
    var type: String? = null

)
