package com.leaderspro.mrlawyer.models

class UserModel {

    companion object {
        const val sharedFileKey = "Login_SP"

        const val COLUMN_ID = "id"
        const val COLUMN_fName = "fname"
        const val COLUMN_lName = "lname"
        const val COLUMN_email = "email"
        const val COLUMN_phone = "phone"
        const val COLUMN_userName = "username"
        const val COLUMN_password = "password"
        const val COLUMN_image = "image"
        const val COLUMN_role = "role"
        const val COLUMN_type = "type"
    }


    var id: String? = null
    var fName: String? = null
    var lName: String? = null
    var email: String? = null
    var phone: String? = null
    var userName: String? = null
    var password: String? = null
    var image: String? = null
    var role: String? = null
    var type: String? = null


}