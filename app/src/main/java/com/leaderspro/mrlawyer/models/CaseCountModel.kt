package com.leaderspro.mrlawyer.models


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CaseCount(
    val `caseCountData`: List<CaseCountData?>?,
    val message: String?,
    val title: String?
)

@JsonClass(generateAdapter = true)
data class CaseCountData(
    @Json(name = "case_count")
    val caseCount: String?
)