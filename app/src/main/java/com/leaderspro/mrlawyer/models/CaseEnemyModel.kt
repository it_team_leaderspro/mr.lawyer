package com.leaderspro.mrlawyer.models

class CaseEnemyModel(
    var id: String? = null,
    var case_id: String? = null,
    var e_name: String? = null,
    var e_sid: String? = null,
    var e_phone: String? = null,
    var e_address: String? = null,
    var adv_name: String? = null,
    var adv_phone: String? = null,
    var adv_address: String? = null,
    var description_id: String? = null,
    var enemieDescription: String? = null

)
