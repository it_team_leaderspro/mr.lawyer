package com.leaderspro.mrlawyer.models

class CaseModel(

    var id: String? = null,
    var client_id: String? = null,
    var clientName: String? = null,
    var court_id: String? = null,
    var type_id: String? = null,
    var strTypeId: String? = null,//case type (نوع القضية)
    var other_type: String? = null,
    var internal_num: String? = null,
    var case_num: String? = null,
    var account_num: String? = null,
    var judge: String? = null,
    var case_type: String? = null,//tvSuitType(نوع الدعوى)
    var case_value: String? = null,
    var date: String? = null,
    var decision: String? = null,
    var status: String? = null,
    var strCaseStatus: String? = null,
    var created: String? = null,
    var courtName: String? = null

)
